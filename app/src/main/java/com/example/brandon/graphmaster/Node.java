package com.example.brandon.graphmaster;

import java.util.HashMap;

/**
 * Created by Brandon on 9/8/2016.
 */
public interface Node {
    public double getValue(HashMap<String, Double> vars) throws Exception;
}
