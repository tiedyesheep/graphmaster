package com.example.brandon.graphmaster;

import android.widget.EditText;

/**
 * Created by Brandon on 11/16/2016.
 */
public class EFunction {
    String name;
    String f;
    Double xmn,xmx,ymn,ymx,step;
    public EFunction(String name, String f, double xmn, double xmx, double ymn, double ymx, double step){
        this.name = name;
        this.f = f;
        this.xmn = xmn;
        this.xmx = xmx;
        this.ymn = ymn;
        this.ymx = ymx;
        this.step = step;
    }
}
