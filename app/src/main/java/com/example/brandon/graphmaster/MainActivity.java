package com.example.brandon.graphmaster;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setTitleTextColor(Color.WHITE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.explicit, menu);
        MenuItem item = menu.findItem(R.id.spinner);
        Spinner spinner = (Spinner) MenuItemCompat.getActionView(item);
        ArrayList<EFunction> functions = new ArrayList<EFunction>();
        try {
            functions.add(new EFunction("Wave", ("(sin(x/5)+cos(y/5))*10"), -15, 15, -15, 15, 3));
            functions.add(new EFunction("Saddle", ("(x^2-y^2)/10"), -15, 15, -15, 15, 3));
            functions.add(new EFunction("Paraboloid", ("(x^2+y^2)/10"), -15, 15, -15, 15, 3));
            functions.add(new EFunction("Black Hole", ("30/((x^2+y^2))"), -15, 15, -15, 15, 1));
            functions.add(new EFunction("Ripple", ("sin((x/5)^2+(y/5)^2)*10"), -15, 15, -15, 15, 1));
        }catch(Exception e){

        }
        ExplicitAdapter adapter = new ExplicitAdapter(this, functions);
        spinner.setAdapter(adapter); // set the adapter to provide layout of rows and content
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                set((EFunction) view.getTag());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return true;
    }


    public void set(EFunction f){
        EditText funct = (EditText)findViewById(R.id.function);
        funct.setText(f.f);
        EditText xmax = (EditText)findViewById(R.id.xmx);
        xmax.setText(Double.toString(f.xmx));
        EditText xmin = (EditText)findViewById(R.id.xmn);
        xmin.setText(Double.toString(f.xmn));
        EditText ymax = (EditText)findViewById(R.id.ymx);
        ymax.setText(Double.toString(f.ymx));
        EditText ymin = (EditText)findViewById(R.id.ymn);
        ymin.setText(Double.toString(f.ymn));
        EditText stp = (EditText)findViewById(R.id.step);
        stp.setText(Double.toString(f.step));
    }
    public void graph(View v){
        boolean parametric = false;
        try{
            EditText funct = (EditText)findViewById(R.id.function);
            String function = funct.getText().toString();
            EditText xmax = (EditText)findViewById(R.id.xmx);
            double xmx = Double.parseDouble(xmax.getText().toString());
            EditText xmin = (EditText)findViewById(R.id.xmn);
            double xmn = Double.parseDouble(xmin.getText().toString());
            EditText ymax = (EditText)findViewById(R.id.ymx);
            double ymx = Double.parseDouble(ymax.getText().toString());
            EditText ymin = (EditText)findViewById(R.id.ymn);
            double ymn = Double.parseDouble(ymin.getText().toString());
            EditText stp = (EditText)findViewById(R.id.step);
            double step = Double.parseDouble(stp.getText().toString());
            CheckBox dot = (CheckBox) findViewById(R.id.dots);
            boolean points = dot.isChecked();
            //CheckBox deri = (CheckBox) findViewById(R.id.der);
            //boolean der = deri.isChecked();
            CheckBox line = (CheckBox) findViewById(R.id.lines);
            boolean lines = line.isChecked();
            boolean valid = true;
            HashMap<String, Double> vars = new HashMap<String, Double>();
            vars.put("x",(xmn));
            vars.put("y",(ymn));
            try {
                translate.nodeFromString(function).getValue(vars);
            }catch(Exception e){
                valid = false;
            }
            if (xmn < xmx && ymn < ymx) {
                if (valid) {
                    if ((xmx - xmn) / step * (ymx - ymn) / step < 10000) {
                        Intent intent = new Intent(this, GraphActivity.class);
                        intent.putExtra("iso", false);
                        intent.putExtra("par", parametric);
                        intent.putExtra("function", function);
                        intent.putExtra("xmx", xmx);
                        intent.putExtra("xmn", xmn);
                        intent.putExtra("ymx", ymx);
                        intent.putExtra("ymn", ymn);
                        intent.putExtra("step", step);
                        intent.putExtra("points", points);
                        intent.putExtra("der", false);
                        intent.putExtra("lines", lines);
                        startActivity(intent);
                    } else {
                        Toast.makeText(MainActivity.this, "Domain too large! try a larger step or smaller bounds.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Invalid Function!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(MainActivity.this, "Min must be smaller than max!", Toast.LENGTH_SHORT).show();
            }
        }catch(Exception e){
            Toast.makeText(MainActivity.this, "Parameters invalid!", Toast.LENGTH_SHORT).show();
        }
    }
    public void par(View v){
        Intent intent = new Intent(this, Parametric.class);
        startActivity(intent);
    }
    public void iso(View v){
        Intent intent = new Intent(this, Implicit.class);
        startActivity(intent);
    }
}
