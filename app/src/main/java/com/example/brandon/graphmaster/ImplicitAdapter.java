package com.example.brandon.graphmaster;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ImplicitAdapter extends ArrayAdapter<IFunction> {
    private final Context context;
    ArrayList<IFunction> values;

    public ImplicitAdapter(Context context, ArrayList<IFunction> values) {

        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount(){
        return values.size();
    }

    @Override
    public View getDropDownView(int position, View view, ViewGroup parent) {
        IFunction choice = (IFunction) getItem(position);

        if (view == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // expand your list item here
            view = vi.inflate(R.layout.textbox, null);
        }
        if(choice != null) {
            // get whatever items are in your view
            TextView text = (TextView) view.findViewById(R.id.namek);
            text.setText(choice.name);
            // do whatever you want with your item view
        }
        return(view);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        System.out.println("c1");
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.textbox, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.namek);
        if(position<values.size()) {
            rowView.setTag(values.get(position));
            textView.setText(values.get(position).name.toString());
        }
        return rowView;
    }
}