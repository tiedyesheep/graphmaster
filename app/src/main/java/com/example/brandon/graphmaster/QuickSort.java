package com.example.brandon.graphmaster;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Brandon on 9/9/2016.
 */
public class QuickSort {
    public static void quicksortz(Point3D[] arr){
        quickSortz(arr, 0, arr.length - 1);
    }
    public static void quickSortz(Point3D[] arr, int low, int high) {
        if (arr == null || arr.length == 0)
            return;

        if (low >= high)
            return;

        // pick the pivot
        int middle = low + (high - low) / 2;
        double pivot = arr[middle].z;

        // make left < pivot and right > pivot
        int i = low, j = high;
        while (i <= j) {
            while (arr[i].z < pivot) {
                i++;
            }

            while (arr[j].z > pivot) {
                j--;
            }

            if (i <= j) {
                Point3D temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
            }
        }

        // recursively sort two sub parts
        if (low < j)
            quickSortz(arr, low, j);

        if (high > i)
            quickSortz(arr, i, high);
    }
    public static void quicksorty(Point3D[] arr){
        quickSorty(arr, 0, arr.length - 1);
    }
    public static void quickSorty(Point3D[] arr, int low, int high) {
        if (arr == null || arr.length == 0)
            return;

        if (low >= high)
            return;

        // pick the pivot
        int middle = low + (high - low) / 2;
        double pivot = arr[middle].y;

        // make left < pivot and right > pivot
        int i = low, j = high;
        while (i <= j) {
            while (arr[i].y < pivot) {
                i++;
            }

            while (arr[j].y > pivot) {
                j--;
            }

            if (i <= j) {
                Point3D temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
            }
        }

        // recursively sort two sub parts
        if (low < j)
            quickSorty(arr, low, j);

        if (high > i)
            quickSorty(arr, i, high);
    }
    public static void quicksortx(Connector[] arr, Point3D[] pos){
        quickSortx(arr,pos, 0, arr.length-1);
    }
    public static void quickSortx(Connector[] arr, Point3D[] pos, int low, int high) {
        if (arr == null || arr.length == 0)
            return;

        if (low >= high)
            return;

        // pick the pivot
        int middle = low + (high - low) / 2;
        double pivot = pos[arr[middle].p1].z;

        // make left < pivot and right > pivot
        int i = low, j = high;
        while (i <= j) {
            while (pos[arr[i].p1].z < pivot) {
                i++;
            }

            while (pos[arr[j].p1].z > pivot) {
                j--;
            }

            if (i <= j) {
                Connector temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
            }
        }

        // recursively sort two sub parts
        if (low < j)
            quickSortx(arr,pos, low, j);

        if (high > i)
            quickSortx(arr,pos, i, high);
    }
}
