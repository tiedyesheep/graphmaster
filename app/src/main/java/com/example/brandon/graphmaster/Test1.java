package com.example.brandon.graphmaster;

/**
 * Created by Brandon on 9/8/2016.
 */
public class Test1 {
    public Test1(){
        double a=1, b=1, c=1;
        double ia=0,ib=0,ic=0;
        double la=0, lb=0, lc=0;
        double cost = 0;
        double lcost = Double.MAX_VALUE;
        //double number = 201;
        double n = 30;
        for(int i = 0; i<100000;i++){
            double a1,b1,c1,a2,b2,c2;
            if(a==0){
                a1 = 0;
            }else{
                a1 = a/abs(a);
            }
            if(b==0){
                b1 = 0;
            }else{
                b1 = b/abs(b);
            }
            if(c==0){
                c1 = 0;
            }else{
                c1 = c/abs(c);
            }
            if(n-2*a-3*b-5*c==0){
                a2 = 0;
                b2 = 0;
                c2 = 0;
            }else{
                a2 = (4 * a + 6 * b + 10 * c - 40) / (abs(n - 2 * a - 3 * b - 5 * c));
                b2 = (6 * a + 9 * b + 15 * c - 60) / (abs(n - 2 * a - 3 * b - 5 * c));
                c2 = (10 * a + 15 * b + 25 * c - 100) / (abs(n - 2 * a - 3 * b - 5 * c));
            }
            if(a1*a2==0){
                ia = 0;
            }else {
                ia = a1 + a2;
            }
            if(b1*b2==0){
                ib = 0;
            }else {
                ib = b1 + b2;
            }
            if(c1*c2==0) {
                ic = 0;
            }else {
                ic = c1 + c2;
            }
            double is = abs(ia+ib+ic);
            ia/=is;
            ib/=is;
            ic/=is;
            a-=0.001*ia;
            b-=0.001*ib;
            c-=0.001*ic;
            cost = abs(a)+abs(b)+abs(c)+abs(n-2*a-3*b-5*c);
            if(cost<lcost){
                lcost = cost;
                la = a;
                lb = b;
                lc = c;
                System.out.println("nl");
            }
            System.out.println(cost);
        }
        System.out.println();
        System.out.println(a+", "+b+", "+c+": "+cost);
        System.out.println(la+", "+lb+", "+lc+": "+lcost);
    }
    public static void main(String[] args){
        new Test1();
    }
    public double abs(double i){
        return Math.abs(i);
    }
}
