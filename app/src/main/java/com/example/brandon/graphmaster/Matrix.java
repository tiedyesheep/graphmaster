package com.example.brandon.graphmaster;

/**
 * Created by Brandon on 10/28/2016.
 */
public class Matrix {
    double[][] n = null;
    int rows = 0;
    int cols = 0;
    public Matrix(double[][] n){
        this.n = n;
        rows = n.length;
        cols = n[0].length;
    }
    public static Matrix multiply(Matrix m1, Matrix m2){
        if(m1.cols==m2.rows) {
            double[][] m = new double[m1.rows][m2.cols];
            for (int a = 0;a<m.length;a++) {
                for(int b = 0;b<m[0].length;b++){
                    double s = 0;
                    for(int i = 0;i<m1.cols;i++){
                        s+=m1.n[a][i]*m2.n[i][b];
                    }
                    m[a][b]=s;
                }
            }
            return new Matrix(m);
        }else{
            return null;
        }
    }
    public static void printMatrix(Matrix m){
        for(int a = 0; a<m.rows; a++){
            for(int b = 0; b<m.cols; b++){
                System.out.print(m.n[a][b]+", ");
            }
            System.out.println();
        }
    }
    public static void main(String[] args){
        double[][] m1d = {{1,0.5}
                        ,{0.5,1}};
        Matrix m1 = new Matrix(m1d);
        Matrix.printMatrix(m1);
        System.out.println();

        double[][] m2d = {{2}
                        ,{1}};
        Matrix m2 = new Matrix(m2d);
        Matrix.printMatrix(m2);
        Matrix m = Matrix.multiply(m1, m2);
        System.out.println();
        printMatrix(m);
    }
}
