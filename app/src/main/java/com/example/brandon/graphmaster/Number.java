package com.example.brandon.graphmaster;

import java.util.HashMap;

/**
 * Created by Brandon on 9/8/2016.
 */
public class Number implements Node {
    double n;
    public Number(double n){
        this.n = n;
    }
    @Override
    public double getValue(HashMap<String, Double> vars){
        return n;
    }
}
