package com.example.brandon.graphmaster;

/**
 * Created by Brandon on 9/14/2016.
 */
public class Timer {
    long start = 0;
    public Timer(){
        start = System.currentTimeMillis();
    }
    public String stop(){
        return Long.toString(System.currentTimeMillis()-start);
    }
}
