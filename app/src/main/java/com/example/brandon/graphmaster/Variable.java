package com.example.brandon.graphmaster;

import java.util.HashMap;

/**
 * Created by Brandon on 9/8/2016.
 */
public class Variable implements Node {
    String letter;
    public Variable(String letter){
        this.letter = letter;
    }
    @Override
    public double getValue(HashMap<String,Double> vars) throws Exception{
        if(vars.containsKey(letter)&&!vars.equals(null)) {
            return vars.get(letter);
        }else{
            throw new Exception();
        }
    }
}
