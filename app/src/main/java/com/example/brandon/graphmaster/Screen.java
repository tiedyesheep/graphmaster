package com.example.brandon.graphmaster;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;
import android.os.AsyncTask;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

/**
 * Created by Brandon on 9/9/2016.
 */
public class Screen extends View {
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    Paint p = new Paint();
    Path path = new Path();
    int mode = NONE;

    // Remember some things for zooming
    PointF last = new PointF();
    PointF start = new PointF();
    float minScale = 0.5f;
    float maxScale = 3f;
    float[] m;
    int viewWidth, viewHeight;
    static final int CLICK = 3;

    float saveScale = 1f;

    protected float origWidth, origHeight;

    int oldMeasuredWidth, oldMeasuredHeight;

    ScaleGestureDetector mScaleDetector;

    Context context;


    boolean points = false;
    boolean lines = false;
    int h = this.getHeight();
    int w = this.getWidth();
    Bitmap pic = Bitmap.createBitmap(100,100, Bitmap.Config.ARGB_8888);
    Screen parent = this;
    Shape shape;
    Shape axes;
    Shape sh;
    Shape s;
    Point3D[] pos;
    Connector[] shapes;
    double rx=0,ry=3,rz=2;
    double sx=10,sy=10,sz=10;
    double dx=0,dy=0,dz=0;
    double iy = 0;

    public Screen(Context context) {
        super(context);
        //axes = shape.axes();
        //initi(shape);
        //sharedConstructing(context);
    }
    public Screen(Context context, Shape shape) {
        super(context);
        this.shape = shape;
        axes = shape.axes();
        initi(shape);
        sharedConstructing(context);
    }

    public void initi(Shape shape){

            int count = 0;
            pos = new Point3D[shape.pos.length + axes.pos.length];
            shapes = new Connector[axes.shapes.length + shape.shapes.length];
        double xmax=0,ymax=0,zmax=0;
        if(shape.pos.length>0) {
            xmax = Math.abs(shape.pos[0].x);
            ymax = Math.abs(shape.pos[0].y);
            zmax = Math.abs(shape.pos[0].z);
        }
            for (int i = 0; i < shape.pos.length; i++) {
                Point3D p = shape.pos[i];
                xmax = (Math.abs(p.x) > xmax) ? Math.abs(p.x) : xmax;
                ymax = (Math.abs(p.y) > ymax) ? Math.abs(p.y) : ymax;
                zmax = (Math.abs(p.z) > zmax) ? Math.abs(p.z) : zmax;
            }
            for (int a = 0; a < shape.pos.length; a++) {
                double x = 0, y = 0, color = 0, color2 = 0, color3 = 0;
                try {
                    x = shape.pos[a].y;
                    y = -shape.pos[a].x;
                    color = shape.pos[a].z;
                    color2 = shape.pos[a].y;
                    color3 = shape.pos[a].x;
                    //System.out.println(color+", "+color2+", "+color3);
                } catch (Exception e) {
                    //System.out.println(i);
                }
                //System.out.println(-color);
                //g.setColor(Color.RED);

                //System.out.println(x+", "+y);
                int r = Math.max(0, Math.min((int) (Math.abs(color) * 255 / zmax), 255));
                int g = Math.max(0, Math.min((int) (Math.abs(color2) * 255 / ymax), 255));
                int b = Math.max(0, Math.min((int) (Math.abs(color3) * 255 / xmax), 255));
                //System.out.println(r+", "+g+", "+b+" REAL");
                pos[a] = (new Point3D(x, y, shape.pos[a].z, Color.rgb(r, g, b)));
                //g.fillOval((int) x, (int) y, 5, 5);
                count++;
            }
            int count1 = 0;
            for (int i = 0; i < shape.shapes.length; i++) {
                shapes[i] = shape.shapes[i];
                count1++;
            }
            Shape a = axes;
            for (int i = 0; i < a.pos.length; i++) {
                double x = a.pos[i].y;
                double y = -a.pos[i].x;
                //g.setColor(Color.RED);

                //System.out.println(x+", "+y);

                pos[i + count] = (new Point3D(x, y, a.pos[i].z, Color.BLACK));
                //g.fillOval((int) x, (int) y, 5, 5);
            }

            for (int i = 0; i < a.shapes.length; i++) {
                shapes[i + count1] = new Connector(a.shapes[i].p1 + count);
            }
            sh = new Shape(pos, shapes);
            s = sh;
            h = this.getHeight();
            w = this.getWidth();
            //System.out.println("draw");

        new DoCalculations().execute();
    }
    /*public Screen(Shape shape){



        //t.start();
    }*/
    @Override
    public void onDraw(Canvas c){
        Timer t = new Timer();
        c.drawBitmap(pic,0,0,p);
        //System.out.println("Draw: "+t.stop());
    }









    double xs = 0, ys = 0;
    /*@Override
    public void mouseDragged(MouseEvent e) {
        rz+=(0.01*(xs-e.getX()));
        ry-=(-0.01*(ys-e.getY()));
        xs = e.getX();
        ys = e.getY();
        invalidate();
        revalidate();
        repaint();
    }*/

    /*@Override
    public void mouseMoved(MouseEvent e) {

    }*/

    /*@Override
    public void mouseClicked(MouseEvent e) {
        xs = e.getX();
        ys = e.getY();
        grabFocus();
    }*/

    /*@Override
    public void mousePressed(MouseEvent e) {
        xs = e.getX();
        ys = e.getY();
        grabFocus();
    }*/

    /*@Override
    public void mouseReleased(MouseEvent e) {
        xs = e.getX();
        ys = e.getY();
    }*/

    /*@Override
    public void mouseEntered(MouseEvent e) {
        xs = e.getX();
        ys = e.getY();
    }*/

    /*@Override
    public void mouseExited(MouseEvent e) {
        xs = e.getX();
        ys = e.getY();
    }*/

    /*@Override
    public void keyTyped(KeyEvent e) {

    }*/

    /*@Override
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode()==KeyEvent.VK_UP){
            dx+=5;
        }else if(e.getKeyCode()==KeyEvent.VK_DOWN){
            dx-=5;
        }else if(e.getKeyCode()==KeyEvent.VK_LEFT){
            dy-=5;
        }else if(e.getKeyCode()==KeyEvent.VK_RIGHT){
            dy+=5;
        }else if(e.getKeyCode()==KeyEvent.VK_SPACE){
            dz+=5;
        }else if(e.getKeyCode()==KeyEvent.VK_SHIFT){
            dz-=5;
        }
        invalidate();
        revalidate();
        repaint();
    }*/

    /*@Override
    public void keyReleased(KeyEvent e) {

    }*/

    /*@Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        sx = Math.pow(sx, 1 + e.getWheelRotation() / 10.0);
        sy = sx;
        sz = sx;
        //System.out.println("mm");
        invalidate();
        revalidate();
        repaint();
    }*/

    /*public class animation implements Runnable{

        @Override
        public void run() {
            double num = 0;
            while(true){
                num+=0.001;
                //sy = 10+Math.sin(num);
                //ry += 0.001;
                //rx+=0.002;
                //ry+=0.003;
                parent.invalidate();
                parent.revalidate();
                parent.repaint();
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }*/


    int asyncs = 0;
    private void sharedConstructing(Context context) {

        super.setClickable(true);

        this.context = context;

        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());

        setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                mScaleDetector.onTouchEvent(event);

                PointF curr = new PointF(event.getX(), event.getY());

                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:

                        last.set(curr);

                        start.set(last);

                        mode = DRAG;

                        break;

                    case MotionEvent.ACTION_MOVE:

                        if (mode == DRAG) {

                            float deltaX = curr.x - last.x;

                            float deltaY = curr.y - last.y;

                            //matrix.postTranslate(fixTransX, fixTransY);

                            rz+=(0.01*(xs-deltaX));
                            ry-=(-0.01*(ys-deltaY));

                            last.set(curr.x, curr.y);

                        }
                        break;

                    case MotionEvent.ACTION_UP:

                        mode = NONE;

                        int xDiff = (int) Math.abs(curr.x - start.x);

                        int yDiff = (int) Math.abs(curr.y - start.y);

                        if (xDiff < CLICK && yDiff < CLICK)

                            performClick();

                        break;

                    case MotionEvent.ACTION_POINTER_UP:

                        mode = NONE;

                        break;

                }
                if(asyncs<=0) {
                    new DoCalculations().execute();
                }

                return true; // indicate event was handled

            }

        });
    }

    public void setMaxZoom(float x) {

        maxScale = x;

    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {

            mode = ZOOM;

            return true;

        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {

            float mScaleFactor = detector.getScaleFactor();
            float direction = detector.getCurrentSpan()-detector.getPreviousSpan();
            int pm = 0;
            if(direction>0){
                pm = 1;
            }else{
                pm = -1;
            }
            float origScale = saveScale;

            saveScale *= mScaleFactor;

            if (saveScale > maxScale) {

                saveScale = maxScale;

                mScaleFactor = maxScale / origScale;

            } else if (saveScale < minScale) {

                saveScale = minScale;

                mScaleFactor = minScale / origScale;

            }

            if (origWidth * saveScale <= viewWidth || origHeight * saveScale <= viewHeight) {

                //matrix.postScale(mScaleFactor, mScaleFactor, viewWidth / 2, viewHeight / 2);
                sx += 0.3*pm*mScaleFactor;
                sy = sx;
                sz = sx;
            }else {
                sx += 0.3*pm*mScaleFactor;
                sy = sx;
                sz = sx;
                //matrix.postScale(mScaleFactor, mScaleFactor, detector.getFocusX(), detector.getFocusY());
            }

            return true;

        }

    }
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        h= MeasureSpec.getSize(heightMeasureSpec);
        w= MeasureSpec.getSize(widthMeasureSpec);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
    private class DoCalculations extends AsyncTask<Void,Shape,Shape> {
        // Do the long-running work in here
        protected Shape doInBackground(Void... v) {
            asyncs++;
            //System.out.println(rx+", "+ry+", "+rz);
            Timer t = new Timer();
            Shape s1 = sh.transform(sx,sy,sz,rx,ry,rz,dx,dy,dz);
            //System.out.println("Calculations: "+t.stop());
            return s1;
        }
        // This is called each time you call publishProgress()
        // This is called when doInBackground() is finished
        protected void onPostExecute(Shape result) {
            //System.out.println("do draw");
            new DoDraw().execute(result);
        }
    }
    private class DoDraw extends AsyncTask<Shape,Double,Bitmap> {
        // Do the long-running work in here
        protected Bitmap doInBackground(Shape... shapes) {

            Timer t = new Timer();
            Shape s = shapes[0];
            Bitmap bm;
            if(w==0||h==0){
                bm = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888);
            }else {
                bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            }
            //System.out.println("Create: "+t.stop());
            Canvas c = new Canvas(bm);

            //Shape s = sh.transform(sx,sy,sz,rx,ry,rz,dx,dy,dz);
            p.setColor(Color.WHITE);
            p.setStyle(Paint.Style.FILL);
            //c.drawRect(new Rect(0,0,bm.getWidth(),bm.getHeight()),p);
            //System.out.println("Clear: "+t.stop());
            int count = 0;
            if(points){
                QuickSort.quicksortz(s.pos);
            }else {
                QuickSort.quicksortx(s.shapes, s.pos);
            }
            //System.out.println("Sort: "+t.stop());
            //QuickSort.quicksortz(pos);
            int x=bm.getWidth()/2;
            int y=bm.getHeight()/2;
            //System.out.println("Before Draw: "+t.stop());
            if (points) {
                for(int i = 0; i<s.pos.length;i++){
                    Point3D cp = s.pos[i];
                    p.setColor(cp.c);
                    p.setStyle(Paint.Style.FILL);
                    c.drawCircle((int)s.pos[i].y+x, (int)s.pos[i].x+y,5,p);
                    //g.fillOval((int)s.pos[i].y+y, (int)s.pos[i].x+x,10,10);
                }
            }else {
                if(lines){
                    p.setStyle(Paint.Style.STROKE);
                }else {
                    p.setStyle(Paint.Style.FILL);
                }
                for (int i = 0; i < s.shapes.length; i++) {
                    Connector cp = s.shapes[i];
                    if (cp.axis) {
                        //p.setShader(null);
                        p.setColor(Color.BLACK);
                        c.drawCircle((int) s.pos[cp.p1].y+x, (int) s.pos[cp.p1].x+y, 5,p);
                        //g.setColor(Color.BLACK);
                        //g.fillOval((int) s.pos[cp.p1].y+y, (int) s.pos[cp.p1].x+x, 10, 10);
                        //System.out.println("axis");
                        //System.out.println(cp.p1);
                    } else {
                        p.setColor(s.pos[cp.p1].c);
                        //Point3D p3 = midpoint(s.pos[cp.p2],s.pos[cp.p3]);
                        //LinearGradient lg = new LinearGradient(0,0,0,h,s.pos[cp.p1].c,p3.c, Shader.TileMode.CLAMP);
                        //g.setColor(s.pos[cp.p1].c);
                        //p.setShader(lg);

                        path.reset();
                        path.moveTo((int) s.pos[cp.p1].y+x, (int) (s.pos[cp.p1].x+y+iy));
                        path.lineTo((int) s.pos[cp.p2].y+x, (int) (s.pos[cp.p2].x+y+iy));
                        path.lineTo((int) s.pos[cp.p3].y+x, (int) (s.pos[cp.p3].x+y+iy));
                        path.lineTo((int) s.pos[cp.p1].y+x, (int) (s.pos[cp.p1].x+y+iy));
                        //System.out.println("Path: "+t.stop());
                        //Polygon p = new Polygon();
                        //p.addPoint((int) s.pos[cp.p1].y+y, (int) (s.pos[cp.p1].x+x+iy));
                        //p.addPoint((int) s.pos[cp.p2].y+y, (int) (s.pos[cp.p2].x+x+iy));
                        //p.addPoint((int) s.pos[cp.p3].y+y, (int) (s.pos[cp.p3].x+x+iy));

                        c.drawPath(path,p);
                        //System.out.println("Draw Path: "+t.stop());
                        //g.fillPolygon(p);
                        //System.out.println(cp.p1);
                        //g.fillOval((int)pos[cp.p1].x,(int)pos[cp.p1].y,10,10);
                    }
                }
            }
            //System.out.println("Drawcalc: "+t.stop());
            return bm;
        }
        // This is called each time you call publishProgress()
        // This is called when doInBackground() is finished
        protected void onPostExecute(Bitmap result) {
            asyncs--;
            pic = result;
            //System.out.println("pic");
            invalidate();
        }
    }
    public static Point3D midpoint(Point3D p1, Point3D p2){
        double x = (p1.x+p2.x)/2;
        double y = (p1.y+p2.y)/2;
        double z = (p1.z+p2.z)/2;
        double r = (Color.red(p1.c)+Color.red(p2.c))/2;
        double g = (Color.green(p1.c)+Color.green(p2.c))/2;
        double b = (Color.blue(p1.c)+Color.blue(p2.c))/2;
        int c = Color.rgb((int)r,(int)g,(int)b);
        return new Point3D(x,y,z,c);
    }

}
