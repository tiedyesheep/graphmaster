package com.example.brandon.graphmaster;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Brandon on 9/9/2016.
 */
public class Shape {
    Point3D[] pos;
    Connector[] shapes;
    public Shape(Point3D[] pos, Connector[] shapes){
        this.pos = pos;
        this.shapes = shapes;
    }
    public Shape rotate(double xd, double yd, double zd){
        Point3D[] pos1 = new Point3D[pos.length];
        for(int i = 0;i<pos.length;i++){
            double a = pos[i].x;
            double b = pos[i].y;
            double c = pos[i].z;

            double tx = (a*cos(zd)-b*sin(zd));
            double ty = (a*sin(zd)+b*cos(zd));
            double tz = c;

            tx = tx;
            ty = ty*cos(xd)-tz*sin(xd);
            tz = ty*sin(xd)+tz*cos(xd);

            tx = tz*cos(yd)-tx*sin(yd);
            ty = ty;
            tz = tz*sin(yd)+tx*cos(yd);

            pos1[i]= new Point3D(tx,ty,tz,pos[0].c);
        }
        return new Shape(pos1, shapes);
    }
    public Shape scale(double xd, double yd, double zd){
        Point3D[] pos1 = new Point3D[pos.length];
        for(int i = 0;i<pos.length;i++){
            try {
                double a = pos[i].x;
                double b = pos[i].y;
                double c = pos[i].z;
                pos1[i] = new Point3D(a * xd, b * yd, c * zd, pos[i].c);
            }catch(Exception e){
                //System.out.println(i);
            }
        }
        return new Shape(pos1, shapes);
    }
    public Shape transform(double sx, double sy, double sz, double rz, double ry, double rx, double dx, double dy, double dz){
        //return scale(sx,sy,sz).rotate(rx,ry,rz);
        //return new Shape(Transform.trans(pos,rx,ry,rz,sx,sy,sz,dx,dy,dz), shapes);
        rx*=-1;
        rz*=-1;
        ry*=-1;
        double sinx = Math.sin(rx);
        double cosx = Math.cos(rx);
        double siny = Math.sin(ry);
        double cosy = Math.cos(ry);
        double sinz = Math.sin(rz);
        double cosz = Math.cos(rz);
        double[][] sd = {{sx,0,0,0}
                        ,{0,sy,0,0}
                        ,{0,0,sz,0}
                        ,{0,0,0,1}};
        Matrix s = new Matrix(sd);
        double[][] xr = {{1,0,0,0}
                ,{0,cosx,-sinx,0}
                ,{0,sinx,cosx,0}
                ,{0,0,0,1}};
        Matrix x = new Matrix(xr);
        double[][] yr = {{cosy,0,siny,0}
                ,{0,1,0,0}
                ,{-siny,0,cosy,0}
                ,{0,0,0,1}};
        Matrix y = new Matrix(yr);
        double[][] zr = {{cosz,-sinz,0,0}
                ,{sinz,cosz,0,0}
                ,{0,0,1,0}
                ,{0,0,0,1}};
        Matrix z = new Matrix(zr);
        Matrix r = Matrix.multiply(z,Matrix.multiply(y,x));
        double[][] dd = {{1,0,0,dx}
                ,{0,1,0,dy}
                ,{0,0,1,dz}
                ,{0,0,0,1}};
        Matrix d = new Matrix(dd);
        Matrix t = Matrix.multiply(s,r);
        double ex = 0;
        double ey = 0;
        double ez = 1;
        /*double[][] hd = {{1,0,-ex/ez,0}
                ,{0,1,-ey/ez,0}
                ,{0,0,1,0}
                ,{0,0,1/ez,0}};*/
        //Matrix h = new Matrix(hd);
        //Matrix t = Matrix.multiply(r,d);
        Point3D[] po = new Point3D[pos.length];
        for(int i = 0; i<pos.length;i++){
            Point3D p = pos[i];
            //Matrix pr = Matrix.multiply(t,p.m);
            Matrix result = Matrix.multiply(t,p.m);
            //System.out.println(result.n[0][0]+", "+result.n[1][0]+", "+result.n[2][0]);
            po[i] = new Point3D(result.n[0][0],result.n[1][0],result.n[2][0], p.c);
        }
        return new Shape(po, this.shapes);
    }
    private double sin(double d){
        return Math.sin(d);
    }
    private double cos(double d){
        return Math.cos(d);
    }
    private double tan(double d){
        return Math.tan(d);
    }
    public static Shape generate(Node n, double xmn, double xmx, double ymn, double ymx, double step, Context prog){
        int length = 0;//(int)Math.ceil(((xmx-xmn)/step+1)*((ymx-ymn)/step+1));
        for(double x = xmn;x<=xmx;x+=step){
            for(double y = ymn;y<=ymx;y+=step){
                length++;
            }
        }
        //System.out.println(length);
        Point3D[] pos = new Point3D[length];
        HashMap<String, Double> vars = new HashMap<String, Double>();
        int count = 0;
        int xl = 0;//(int)((xmx-xmn)/step+1);
        for(double i = xmn;i<=xmx;i+=step){
            xl++;
        }
        int yl = 0;//(int)((ymx-ymn)/step+1);
        for(double i = ymn;i<=ymx;i+=step){
            yl++;
        }
        int xp1 = 0;
        int yp1 = 0;
        for(double x = xmn;x<=xmx;x+=step){
            yp1=0;
            for(double y = ymn;y<=ymx;y+=step){
                vars.put("x",x);
                vars.put("y",y);
                try {
                    pos[count]=new Point3D(x,y,n.getValue(vars), Color.BLACK);
                    //System.out.println(xp1+", "+yp1+": "+xl+", "+yl+" outlook: "+count);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                count++;
                yp1++;
            }
            xp1++;
        }
        int counter = 0;
        int xp = 0;
        int yp = 0;
        xp++;
        for(double x = xmn+step;x<=xmx-step;x+=step){
            for(double y = ymn+step;y<=ymx-step;y+=step){
                counter++;
            }
        }
        counter*=4;
        Connector[] shape = new Connector[0];
        if(xl>2&&yl>2) {
            counter += (4 * (xl - 2)) + (4 * (yl - 2)) + 4;
            shape = new Connector[counter];
            counter = 0;
            for (double x = xmn + step; x <= xmx - step; x += step) {
                yp++;
                for (double y = ymn + step; y <= ymx - step; y += step) {
                    //System.out.println(xp+", "+yp);
                    shape[counter] = new Connector(getPos(xl, yl, xp, yp), getPos(xl, yl, xp + 1, yp), getPos(xl, yl, xp, yp + 1));
                    counter++;
                    shape[counter] = new Connector(getPos(xl, yl, xp, yp), getPos(xl, yl, xp - 1, yp), getPos(xl, yl, xp, yp + 1));
                    counter++;
                    shape[counter] = new Connector(getPos(xl, yl, xp, yp), getPos(xl, yl, xp + 1, yp), getPos(xl, yl, xp, yp - 1));
                    counter++;
                    shape[counter] = new Connector(getPos(xl, yl, xp, yp), getPos(xl, yl, xp - 1, yp), getPos(xl, yl, xp, yp - 1));
                    counter++;
                    yp++;
                }
                yp = 0;
                xp++;
            }
            yp = 0;
            xp = 0;
            yp++;
            for (double y = ymn + step; y <= ymx - step; y += step) {
                //System.out.println(xp+", "+yp);
                shape[counter] = new Connector(getPos(xl, yl, xp, yp), getPos(xl, yl, xp + 1, yp), getPos(xl, yl, xp, yp + 1));
                counter++;
                shape[counter] = new Connector(getPos(xl, yl, xp, yp), getPos(xl, yl, xp + 1, yp), getPos(xl, yl, xp, yp - 1));
                counter++;
                yp++;
            }
            yp = 0;
            xp = xl - 1;
            yp++;
            for (double y = ymn + step; y <= ymx - step; y += step) {
                //System.out.println(xp+", "+yp);
                shape[counter] = new Connector(getPos(xl, yl, xp, yp), getPos(xl, yl, xp - 1, yp), getPos(xl, yl, xp, yp + 1));
                counter++;
                shape[counter] = new Connector(getPos(xl, yl, xp, yp), getPos(xl, yl, xp - 1, yp), getPos(xl, yl, xp, yp - 1));
                counter++;
                yp++;
            }
            yp = 0;
            xp = 0;
            xp++;
            for (double x = xmn + step; x <= xmx - step; x += step) {
                //System.out.println(xp+", "+yp);
                shape[counter] = new Connector(getPos(xl, yl, xp, yp), getPos(xl, yl, xp + 1, yp), getPos(xl, yl, xp, yp + 1));
                counter++;
                shape[counter] = new Connector(getPos(xl, yl, xp, yp), getPos(xl, yl, xp - 1, yp), getPos(xl, yl, xp, yp + 1));
                counter++;
                xp++;
            }
            yp = yl - 1;
            xp = 0;
            xp++;
            for (double x = xmn + step; x <= xmx - step; x += step) {
                //System.out.println(xp+", "+yp);
                shape[counter] = new Connector(getPos(xl, yl, xp, yp), getPos(xl, yl, xp + 1, yp), getPos(xl, yl, xp, yp - 1));
                counter++;
                shape[counter] = new Connector(getPos(xl, yl, xp, yp), getPos(xl, yl, xp - 1, yp), getPos(xl, yl, xp, yp - 1));
                counter++;
                xp++;
            }
            xp = 0;
            yp = 0;
            shape[counter] = new Connector(getPos(xl, yl, xp, yp), getPos(xl, yl, xp + 1, yp), getPos(xl, yl, xp, yp + 1));
            counter++;

            xp = xl - 1;
            yp = 0;
            shape[counter] = new Connector(getPos(xl, yl, xp, yp), getPos(xl, yl, xp - 1, yp), getPos(xl, yl, xp, yp + 1));
            counter++;

            xp = 0;
            yp = yl - 1;
            shape[counter] = new Connector(getPos(xl, yl, xp, yp), getPos(xl, yl, xp + 1, yp), getPos(xl, yl, xp, yp - 1));
            counter++;

            xp = xl - 1;
            yp = yl - 1;
            shape[counter] = new Connector(getPos(xl, yl, xp, yp), getPos(xl, yl, xp - 1, yp), getPos(xl, yl, xp, yp - 1));
            counter++;
        }
        return new Shape(pos, shape);
    }

    public static Shape generatep(Node xn, Node yn, Node zn, double umn, double umx, double vmn, double vmx, double step, Context prog){
        int length = 0;//(int)Math.ceil(((umx-umn)/step+1)*((vmx-vmn)/step+1));
        for(double u = umn;u<=umx;u+=step){
            for(double v = vmn;v<=vmx;v+=step){
                length++;
            }
        }
        //System.out.println(length);
        Point3D[] pos = new Point3D[length];
        HashMap<String, Double> vars = new HashMap<String, Double>();
        int count = 0;
        int ul = 0;//(int)((umx-umn)/step+1);
        for(double u = umn;u<=umx;u+=step){
            ul++;
        }
        int vl = 0;//(int)((vmx-vmn)/step+1);
        for(double v = vmn;v<=vmx;v+=step){
            vl++;
        }
        for(double u = umn;u<=umx;u+=step){
            for(double v = vmn;v<=vmx;v+=step){
                vars.put("u",u);
                vars.put("v",v);
                try {
                    pos[count]=new Point3D(xn.getValue(vars),yn.getValue(vars),zn.getValue(vars), Color.BLACK);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                count++;
            }
        }
        int counter = 0;
        int xp = 0;
        int yp = 0;
        xp++;
        for(double u = umn+step;u<=umx-step;u+=step){
            for(double v = vmn+step;v<=vmx-step;v+=step){
                counter++;
            }
        }
        counter*=4;
        ArrayList<Connector> shp = new ArrayList<Connector>();
        Connector[] shape = new Connector[0];
        if(ul>2&&vl>2) {
            counter += (4 * (ul - 2)) + (4 * (vl - 2)) + 4;
            shape = new Connector[counter];
            counter = 0;
            System.out.println("p3: "+counter);
            for (double u = umn + step; u <= umx - step; u += step) {
                yp++;
                for (double v = vmn + step; v <= vmx - step; v += step) {
                    //System.out.println(xp+", "+yp);
                    shp.add(new Connector(getPos(ul, vl, xp, yp), getPos(ul, vl, xp + 1, yp), getPos(ul, vl, xp, yp + 1)));
                    counter++;
                    shp.add(new Connector(getPos(ul, vl, xp, yp), getPos(ul, vl, xp - 1, yp), getPos(ul, vl, xp, yp + 1)));
                    counter++;
                    shp.add(new Connector(getPos(ul, vl, xp, yp), getPos(ul, vl, xp + 1, yp), getPos(ul, vl, xp, yp - 1)));
                    counter++;
                    shp.add(new Connector(getPos(ul, vl, xp, yp), getPos(ul, vl, xp - 1, yp), getPos(ul, vl, xp, yp - 1)));
                    counter++;
                    yp++;
                }
                yp = 0;
                xp++;
            }
            yp = 0;
            xp = 0;
            yp++;
            System.out.println("p4: "+counter);
            for (double y = vmn + step; y <= vmx - step; y += step) {
                //System.out.println(xp+", "+yp);
                shp.add(new Connector(getPos(ul, vl, xp, yp), getPos(ul, vl, xp + 1, yp), getPos(ul, vl, xp, yp + 1)));
                counter++;
                shp.add(new Connector(getPos(ul, vl, xp, yp), getPos(ul, vl, xp + 1, yp), getPos(ul, vl, xp, yp - 1)));
                counter++;
                yp++;
            }
            yp = 0;
            xp = ul - 1;
            yp++;
            System.out.println("p5: "+counter);
            for (double y = vmn + step; y <= vmx - step; y += step) {
                //System.out.println(xp+", "+yp);
                shp.add(new Connector(getPos(ul, vl, xp, yp), getPos(ul, vl, xp - 1, yp), getPos(ul, vl, xp, yp + 1)));
                counter++;
                shp.add(new Connector(getPos(ul, vl, xp, yp), getPos(ul, vl, xp - 1, yp), getPos(ul, vl, xp, yp - 1)));
                counter++;
                yp++;
            }
            yp = 0;
            xp = 0;
            xp++;
            System.out.println("p6: "+counter);
            for (double x = umn + step; x <= umx - step; x += step) {
                //System.out.println(xp+", "+yp);
                shp.add(new Connector(getPos(ul, vl, xp, yp), getPos(ul, vl, xp + 1, yp), getPos(ul, vl, xp, yp + 1)));
                counter++;
                shp.add(new Connector(getPos(ul, vl, xp, yp), getPos(ul, vl, xp - 1, yp), getPos(ul, vl, xp, yp + 1)));
                counter++;
                xp++;
            }
            yp = vl - 1;
            xp = 0;
            xp++;
            System.out.println("p7: "+counter);
            for (double x = umn + step; x <= umx - step; x += step) {
                //System.out.println(xp+", "+yp);
                shp.add(new Connector(getPos(ul, vl, xp, yp), getPos(ul, vl, xp + 1, yp), getPos(ul, vl, xp, yp - 1)));
                counter++;
                shp.add(new Connector(getPos(ul, vl, xp, yp), getPos(ul, vl, xp - 1, yp), getPos(ul, vl, xp, yp - 1)));
                counter++;
                xp++;
            }
            System.out.println("p8: "+counter);
            xp = 0;
            yp = 0;
            shp.add(new Connector(getPos(ul, vl, xp, yp), getPos(ul, vl, xp + 1, yp), getPos(ul, vl, xp, yp + 1)));
            counter++;
            System.out.println("p9: "+counter);

            xp = ul - 1;
            yp = 0;
            shp.add(new Connector(getPos(ul, vl, xp, yp), getPos(ul, vl, xp - 1, yp), getPos(ul, vl, xp, yp + 1)));
            counter++;
            System.out.println("p10: "+counter);

            xp = 0;
            yp = vl - 1;
            shp.add(new Connector(getPos(ul, vl, xp, yp), getPos(ul, vl, xp + 1, yp), getPos(ul, vl, xp, yp - 1)));
            counter++;
            System.out.println("p11: "+counter);

            xp = ul - 1;
            yp = vl - 1;
            shp.add(new Connector(getPos(ul, vl, xp, yp), getPos(ul, vl, xp - 1, yp), getPos(ul, vl, xp, yp - 1)));
            counter++;
        }
        Connector[] shpe = shp.toArray(new Connector[shp.size()]);
        return new Shape(pos, shpe);
    }


    static int[] edgeTable = {0x0, 0x109, 0x203, 0x30a, 0x406, 0x50f, 0x605, 0x70c,
            0x80c, 0x905, 0xa0f, 0xb06, 0xc0a, 0xd03, 0xe09, 0xf00,
            0x190, 0x99, 0x393, 0x29a, 0x596, 0x49f, 0x795, 0x69c,
            0x99c, 0x895, 0xb9f, 0xa96, 0xd9a, 0xc93, 0xf99, 0xe90,
            0x230, 0x339, 0x33, 0x13a, 0x636, 0x73f, 0x435, 0x53c,
            0xa3c, 0xb35, 0x83f, 0x936, 0xe3a, 0xf33, 0xc39, 0xd30,
            0x3a0, 0x2a9, 0x1a3, 0xaa, 0x7a6, 0x6af, 0x5a5, 0x4ac,
            0xbac, 0xaa5, 0x9af, 0x8a6, 0xfaa, 0xea3, 0xda9, 0xca0,
            0x460, 0x569, 0x663, 0x76a, 0x66, 0x16f, 0x265, 0x36c,
            0xc6c, 0xd65, 0xe6f, 0xf66, 0x86a, 0x963, 0xa69, 0xb60,
            0x5f0, 0x4f9, 0x7f3, 0x6fa, 0x1f6, 0xff, 0x3f5, 0x2fc,
            0xdfc, 0xcf5, 0xfff, 0xef6, 0x9fa, 0x8f3, 0xbf9, 0xaf0,
            0x650, 0x759, 0x453, 0x55a, 0x256, 0x35f, 0x55, 0x15c,
            0xe5c, 0xf55, 0xc5f, 0xd56, 0xa5a, 0xb53, 0x859, 0x950,
            0x7c0, 0x6c9, 0x5c3, 0x4ca, 0x3c6, 0x2cf, 0x1c5, 0xcc,
            0xfcc, 0xec5, 0xdcf, 0xcc6, 0xbca, 0xac3, 0x9c9, 0x8c0,
            0x8c0, 0x9c9, 0xac3, 0xbca, 0xcc6, 0xdcf, 0xec5, 0xfcc,
            0xcc, 0x1c5, 0x2cf, 0x3c6, 0x4ca, 0x5c3, 0x6c9, 0x7c0,
            0x950, 0x859, 0xb53, 0xa5a, 0xd56, 0xc5f, 0xf55, 0xe5c,
            0x15c, 0x55, 0x35f, 0x256, 0x55a, 0x453, 0x759, 0x650,
            0xaf0, 0xbf9, 0x8f3, 0x9fa, 0xef6, 0xfff, 0xcf5, 0xdfc,
            0x2fc, 0x3f5, 0xff, 0x1f6, 0x6fa, 0x7f3, 0x4f9, 0x5f0,
            0xb60, 0xa69, 0x963, 0x86a, 0xf66, 0xe6f, 0xd65, 0xc6c,
            0x36c, 0x265, 0x16f, 0x66, 0x76a, 0x663, 0x569, 0x460,
            0xca0, 0xda9, 0xea3, 0xfaa, 0x8a6, 0x9af, 0xaa5, 0xbac,
            0x4ac, 0x5a5, 0x6af, 0x7a6, 0xaa, 0x1a3, 0x2a9, 0x3a0,
            0xd30, 0xc39, 0xf33, 0xe3a, 0x936, 0x83f, 0xb35, 0xa3c,
            0x53c, 0x435, 0x73f, 0x636, 0x13a, 0x33, 0x339, 0x230,
            0xe90, 0xf99, 0xc93, 0xd9a, 0xa96, 0xb9f, 0x895, 0x99c,
            0x69c, 0x795, 0x49f, 0x596, 0x29a, 0x393, 0x99, 0x190,
            0xf00, 0xe09, 0xd03, 0xc0a, 0xb06, 0xa0f, 0x905, 0x80c,
            0x70c, 0x605, 0x50f, 0x406, 0x30a, 0x203, 0x109, 0x0};

    static int[][] triTable = {{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {0, 1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {1, 8, 3, 9, 8, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {0, 8, 3, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {9, 2, 10, 0, 2, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {2, 8, 3, 2, 10, 8, 10, 9, 8, -1, -1, -1, -1, -1, -1, -1},
            {3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {0, 11, 2, 8, 11, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {1, 9, 0, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {1, 11, 2, 1, 9, 11, 9, 8, 11, -1, -1, -1, -1, -1, -1, -1},
            {3, 10, 1, 11, 10, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {0, 10, 1, 0, 8, 10, 8, 11, 10, -1, -1, -1, -1, -1, -1, -1},
            {3, 9, 0, 3, 11, 9, 11, 10, 9, -1, -1, -1, -1, -1, -1, -1},
            {9, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {4, 3, 0, 7, 3, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {0, 1, 9, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {4, 1, 9, 4, 7, 1, 7, 3, 1, -1, -1, -1, -1, -1, -1, -1},
            {1, 2, 10, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {3, 4, 7, 3, 0, 4, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1},
            {9, 2, 10, 9, 0, 2, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1},
            {2, 10, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4, -1, -1, -1, -1},
            {8, 4, 7, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {11, 4, 7, 11, 2, 4, 2, 0, 4, -1, -1, -1, -1, -1, -1, -1},
            {9, 0, 1, 8, 4, 7, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1},
            {4, 7, 11, 9, 4, 11, 9, 11, 2, 9, 2, 1, -1, -1, -1, -1},
            {3, 10, 1, 3, 11, 10, 7, 8, 4, -1, -1, -1, -1, -1, -1, -1},
            {1, 11, 10, 1, 4, 11, 1, 0, 4, 7, 11, 4, -1, -1, -1, -1},
            {4, 7, 8, 9, 0, 11, 9, 11, 10, 11, 0, 3, -1, -1, -1, -1},
            {4, 7, 11, 4, 11, 9, 9, 11, 10, -1, -1, -1, -1, -1, -1, -1},
            {9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {9, 5, 4, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {0, 5, 4, 1, 5, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {8, 5, 4, 8, 3, 5, 3, 1, 5, -1, -1, -1, -1, -1, -1, -1},
            {1, 2, 10, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {3, 0, 8, 1, 2, 10, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1},
            {5, 2, 10, 5, 4, 2, 4, 0, 2, -1, -1, -1, -1, -1, -1, -1},
            {2, 10, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8, -1, -1, -1, -1},
            {9, 5, 4, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {0, 11, 2, 0, 8, 11, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1},
            {0, 5, 4, 0, 1, 5, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1},
            {2, 1, 5, 2, 5, 8, 2, 8, 11, 4, 8, 5, -1, -1, -1, -1},
            {10, 3, 11, 10, 1, 3, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1},
            {4, 9, 5, 0, 8, 1, 8, 10, 1, 8, 11, 10, -1, -1, -1, -1},
            {5, 4, 0, 5, 0, 11, 5, 11, 10, 11, 0, 3, -1, -1, -1, -1},
            {5, 4, 8, 5, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1},
            {9, 7, 8, 5, 7, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {9, 3, 0, 9, 5, 3, 5, 7, 3, -1, -1, -1, -1, -1, -1, -1},
            {0, 7, 8, 0, 1, 7, 1, 5, 7, -1, -1, -1, -1, -1, -1, -1},
            {1, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {9, 7, 8, 9, 5, 7, 10, 1, 2, -1, -1, -1, -1, -1, -1, -1},
            {10, 1, 2, 9, 5, 0, 5, 3, 0, 5, 7, 3, -1, -1, -1, -1},
            {8, 0, 2, 8, 2, 5, 8, 5, 7, 10, 5, 2, -1, -1, -1, -1},
            {2, 10, 5, 2, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1},
            {7, 9, 5, 7, 8, 9, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1},
            {9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 11, -1, -1, -1, -1},
            {2, 3, 11, 0, 1, 8, 1, 7, 8, 1, 5, 7, -1, -1, -1, -1},
            {11, 2, 1, 11, 1, 7, 7, 1, 5, -1, -1, -1, -1, -1, -1, -1},
            {9, 5, 8, 8, 5, 7, 10, 1, 3, 10, 3, 11, -1, -1, -1, -1},
            {5, 7, 0, 5, 0, 9, 7, 11, 0, 1, 0, 10, 11, 10, 0, -1},
            {11, 10, 0, 11, 0, 3, 10, 5, 0, 8, 0, 7, 5, 7, 0, -1},
            {11, 10, 5, 7, 11, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {0, 8, 3, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {9, 0, 1, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {1, 8, 3, 1, 9, 8, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1},
            {1, 6, 5, 2, 6, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {1, 6, 5, 1, 2, 6, 3, 0, 8, -1, -1, -1, -1, -1, -1, -1},
            {9, 6, 5, 9, 0, 6, 0, 2, 6, -1, -1, -1, -1, -1, -1, -1},
            {5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8, -1, -1, -1, -1},
            {2, 3, 11, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {11, 0, 8, 11, 2, 0, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1},
            {0, 1, 9, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1},
            {5, 10, 6, 1, 9, 2, 9, 11, 2, 9, 8, 11, -1, -1, -1, -1},
            {6, 3, 11, 6, 5, 3, 5, 1, 3, -1, -1, -1, -1, -1, -1, -1},
            {0, 8, 11, 0, 11, 5, 0, 5, 1, 5, 11, 6, -1, -1, -1, -1},
            {3, 11, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9, -1, -1, -1, -1},
            {6, 5, 9, 6, 9, 11, 11, 9, 8, -1, -1, -1, -1, -1, -1, -1},
            {5, 10, 6, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {4, 3, 0, 4, 7, 3, 6, 5, 10, -1, -1, -1, -1, -1, -1, -1},
            {1, 9, 0, 5, 10, 6, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1},
            {10, 6, 5, 1, 9, 7, 1, 7, 3, 7, 9, 4, -1, -1, -1, -1},
            {6, 1, 2, 6, 5, 1, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1},
            {1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7, -1, -1, -1, -1},
            {8, 4, 7, 9, 0, 5, 0, 6, 5, 0, 2, 6, -1, -1, -1, -1},
            {7, 3, 9, 7, 9, 4, 3, 2, 9, 5, 9, 6, 2, 6, 9, -1},
            {3, 11, 2, 7, 8, 4, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1},
            {5, 10, 6, 4, 7, 2, 4, 2, 0, 2, 7, 11, -1, -1, -1, -1},
            {0, 1, 9, 4, 7, 8, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1},
            {9, 2, 1, 9, 11, 2, 9, 4, 11, 7, 11, 4, 5, 10, 6, -1},
            {8, 4, 7, 3, 11, 5, 3, 5, 1, 5, 11, 6, -1, -1, -1, -1},
            {5, 1, 11, 5, 11, 6, 1, 0, 11, 7, 11, 4, 0, 4, 11, -1},
            {0, 5, 9, 0, 6, 5, 0, 3, 6, 11, 6, 3, 8, 4, 7, -1},
            {6, 5, 9, 6, 9, 11, 4, 7, 9, 7, 11, 9, -1, -1, -1, -1},
            {10, 4, 9, 6, 4, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {4, 10, 6, 4, 9, 10, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1},
            {10, 0, 1, 10, 6, 0, 6, 4, 0, -1, -1, -1, -1, -1, -1, -1},
            {8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 10, -1, -1, -1, -1},
            {1, 4, 9, 1, 2, 4, 2, 6, 4, -1, -1, -1, -1, -1, -1, -1},
            {3, 0, 8, 1, 2, 9, 2, 4, 9, 2, 6, 4, -1, -1, -1, -1},
            {0, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {8, 3, 2, 8, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1},
            {10, 4, 9, 10, 6, 4, 11, 2, 3, -1, -1, -1, -1, -1, -1, -1},
            {0, 8, 2, 2, 8, 11, 4, 9, 10, 4, 10, 6, -1, -1, -1, -1},
            {3, 11, 2, 0, 1, 6, 0, 6, 4, 6, 1, 10, -1, -1, -1, -1},
            {6, 4, 1, 6, 1, 10, 4, 8, 1, 2, 1, 11, 8, 11, 1, -1},
            {9, 6, 4, 9, 3, 6, 9, 1, 3, 11, 6, 3, -1, -1, -1, -1},
            {8, 11, 1, 8, 1, 0, 11, 6, 1, 9, 1, 4, 6, 4, 1, -1},
            {3, 11, 6, 3, 6, 0, 0, 6, 4, -1, -1, -1, -1, -1, -1, -1},
            {6, 4, 8, 11, 6, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {7, 10, 6, 7, 8, 10, 8, 9, 10, -1, -1, -1, -1, -1, -1, -1},
            {0, 7, 3, 0, 10, 7, 0, 9, 10, 6, 7, 10, -1, -1, -1, -1},
            {10, 6, 7, 1, 10, 7, 1, 7, 8, 1, 8, 0, -1, -1, -1, -1},
            {10, 6, 7, 10, 7, 1, 1, 7, 3, -1, -1, -1, -1, -1, -1, -1},
            {1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7, -1, -1, -1, -1},
            {2, 6, 9, 2, 9, 1, 6, 7, 9, 0, 9, 3, 7, 3, 9, -1},
            {7, 8, 0, 7, 0, 6, 6, 0, 2, -1, -1, -1, -1, -1, -1, -1},
            {7, 3, 2, 6, 7, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {2, 3, 11, 10, 6, 8, 10, 8, 9, 8, 6, 7, -1, -1, -1, -1},
            {2, 0, 7, 2, 7, 11, 0, 9, 7, 6, 7, 10, 9, 10, 7, -1},
            {1, 8, 0, 1, 7, 8, 1, 10, 7, 6, 7, 10, 2, 3, 11, -1},
            {11, 2, 1, 11, 1, 7, 10, 6, 1, 6, 7, 1, -1, -1, -1, -1},
            {8, 9, 6, 8, 6, 7, 9, 1, 6, 11, 6, 3, 1, 3, 6, -1},
            {0, 9, 1, 11, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {7, 8, 0, 7, 0, 6, 3, 11, 0, 11, 6, 0, -1, -1, -1, -1},
            {7, 11, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {3, 0, 8, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {0, 1, 9, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {8, 1, 9, 8, 3, 1, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1},
            {10, 1, 2, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {1, 2, 10, 3, 0, 8, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1},
            {2, 9, 0, 2, 10, 9, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1},
            {6, 11, 7, 2, 10, 3, 10, 8, 3, 10, 9, 8, -1, -1, -1, -1},
            {7, 2, 3, 6, 2, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {7, 0, 8, 7, 6, 0, 6, 2, 0, -1, -1, -1, -1, -1, -1, -1},
            {2, 7, 6, 2, 3, 7, 0, 1, 9, -1, -1, -1, -1, -1, -1, -1},
            {1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6, -1, -1, -1, -1},
            {10, 7, 6, 10, 1, 7, 1, 3, 7, -1, -1, -1, -1, -1, -1, -1},
            {10, 7, 6, 1, 7, 10, 1, 8, 7, 1, 0, 8, -1, -1, -1, -1},
            {0, 3, 7, 0, 7, 10, 0, 10, 9, 6, 10, 7, -1, -1, -1, -1},
            {7, 6, 10, 7, 10, 8, 8, 10, 9, -1, -1, -1, -1, -1, -1, -1},
            {6, 8, 4, 11, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {3, 6, 11, 3, 0, 6, 0, 4, 6, -1, -1, -1, -1, -1, -1, -1},
            {8, 6, 11, 8, 4, 6, 9, 0, 1, -1, -1, -1, -1, -1, -1, -1},
            {9, 4, 6, 9, 6, 3, 9, 3, 1, 11, 3, 6, -1, -1, -1, -1},
            {6, 8, 4, 6, 11, 8, 2, 10, 1, -1, -1, -1, -1, -1, -1, -1},
            {1, 2, 10, 3, 0, 11, 0, 6, 11, 0, 4, 6, -1, -1, -1, -1},
            {4, 11, 8, 4, 6, 11, 0, 2, 9, 2, 10, 9, -1, -1, -1, -1},
            {10, 9, 3, 10, 3, 2, 9, 4, 3, 11, 3, 6, 4, 6, 3, -1},
            {8, 2, 3, 8, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1},
            {0, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {1, 9, 0, 2, 3, 4, 2, 4, 6, 4, 3, 8, -1, -1, -1, -1},
            {1, 9, 4, 1, 4, 2, 2, 4, 6, -1, -1, -1, -1, -1, -1, -1},
            {8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 10, 1, -1, -1, -1, -1},
            {10, 1, 0, 10, 0, 6, 6, 0, 4, -1, -1, -1, -1, -1, -1, -1},
            {4, 6, 3, 4, 3, 8, 6, 10, 3, 0, 3, 9, 10, 9, 3, -1},
            {10, 9, 4, 6, 10, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {4, 9, 5, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {0, 8, 3, 4, 9, 5, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1},
            {5, 0, 1, 5, 4, 0, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1},
            {11, 7, 6, 8, 3, 4, 3, 5, 4, 3, 1, 5, -1, -1, -1, -1},
            {9, 5, 4, 10, 1, 2, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1},
            {6, 11, 7, 1, 2, 10, 0, 8, 3, 4, 9, 5, -1, -1, -1, -1},
            {7, 6, 11, 5, 4, 10, 4, 2, 10, 4, 0, 2, -1, -1, -1, -1},
            {3, 4, 8, 3, 5, 4, 3, 2, 5, 10, 5, 2, 11, 7, 6, -1},
            {7, 2, 3, 7, 6, 2, 5, 4, 9, -1, -1, -1, -1, -1, -1, -1},
            {9, 5, 4, 0, 8, 6, 0, 6, 2, 6, 8, 7, -1, -1, -1, -1},
            {3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0, -1, -1, -1, -1},
            {6, 2, 8, 6, 8, 7, 2, 1, 8, 4, 8, 5, 1, 5, 8, -1},
            {9, 5, 4, 10, 1, 6, 1, 7, 6, 1, 3, 7, -1, -1, -1, -1},
            {1, 6, 10, 1, 7, 6, 1, 0, 7, 8, 7, 0, 9, 5, 4, -1},
            {4, 0, 10, 4, 10, 5, 0, 3, 10, 6, 10, 7, 3, 7, 10, -1},
            {7, 6, 10, 7, 10, 8, 5, 4, 10, 4, 8, 10, -1, -1, -1, -1},
            {6, 9, 5, 6, 11, 9, 11, 8, 9, -1, -1, -1, -1, -1, -1, -1},
            {3, 6, 11, 0, 6, 3, 0, 5, 6, 0, 9, 5, -1, -1, -1, -1},
            {0, 11, 8, 0, 5, 11, 0, 1, 5, 5, 6, 11, -1, -1, -1, -1},
            {6, 11, 3, 6, 3, 5, 5, 3, 1, -1, -1, -1, -1, -1, -1, -1},
            {1, 2, 10, 9, 5, 11, 9, 11, 8, 11, 5, 6, -1, -1, -1, -1},
            {0, 11, 3, 0, 6, 11, 0, 9, 6, 5, 6, 9, 1, 2, 10, -1},
            {11, 8, 5, 11, 5, 6, 8, 0, 5, 10, 5, 2, 0, 2, 5, -1},
            {6, 11, 3, 6, 3, 5, 2, 10, 3, 10, 5, 3, -1, -1, -1, -1},
            {5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2, -1, -1, -1, -1},
            {9, 5, 6, 9, 6, 0, 0, 6, 2, -1, -1, -1, -1, -1, -1, -1},
            {1, 5, 8, 1, 8, 0, 5, 6, 8, 3, 8, 2, 6, 2, 8, -1},
            {1, 5, 6, 2, 1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {1, 3, 6, 1, 6, 10, 3, 8, 6, 5, 6, 9, 8, 9, 6, -1},
            {10, 1, 0, 10, 0, 6, 9, 5, 0, 5, 6, 0, -1, -1, -1, -1},
            {0, 3, 8, 5, 6, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {10, 5, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {11, 5, 10, 7, 5, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {11, 5, 10, 11, 7, 5, 8, 3, 0, -1, -1, -1, -1, -1, -1, -1},
            {5, 11, 7, 5, 10, 11, 1, 9, 0, -1, -1, -1, -1, -1, -1, -1},
            {10, 7, 5, 10, 11, 7, 9, 8, 1, 8, 3, 1, -1, -1, -1, -1},
            {11, 1, 2, 11, 7, 1, 7, 5, 1, -1, -1, -1, -1, -1, -1, -1},
            {0, 8, 3, 1, 2, 7, 1, 7, 5, 7, 2, 11, -1, -1, -1, -1},
            {9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 11, 7, -1, -1, -1, -1},
            {7, 5, 2, 7, 2, 11, 5, 9, 2, 3, 2, 8, 9, 8, 2, -1},
            {2, 5, 10, 2, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1},
            {8, 2, 0, 8, 5, 2, 8, 7, 5, 10, 2, 5, -1, -1, -1, -1},
            {9, 0, 1, 5, 10, 3, 5, 3, 7, 3, 10, 2, -1, -1, -1, -1},
            {9, 8, 2, 9, 2, 1, 8, 7, 2, 10, 2, 5, 7, 5, 2, -1},
            {1, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {0, 8, 7, 0, 7, 1, 1, 7, 5, -1, -1, -1, -1, -1, -1, -1},
            {9, 0, 3, 9, 3, 5, 5, 3, 7, -1, -1, -1, -1, -1, -1, -1},
            {9, 8, 7, 5, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {5, 8, 4, 5, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1},
            {5, 0, 4, 5, 11, 0, 5, 10, 11, 11, 3, 0, -1, -1, -1, -1},
            {0, 1, 9, 8, 4, 10, 8, 10, 11, 10, 4, 5, -1, -1, -1, -1},
            {10, 11, 4, 10, 4, 5, 11, 3, 4, 9, 4, 1, 3, 1, 4, -1},
            {2, 5, 1, 2, 8, 5, 2, 11, 8, 4, 5, 8, -1, -1, -1, -1},
            {0, 4, 11, 0, 11, 3, 4, 5, 11, 2, 11, 1, 5, 1, 11, -1},
            {0, 2, 5, 0, 5, 9, 2, 11, 5, 4, 5, 8, 11, 8, 5, -1},
            {9, 4, 5, 2, 11, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {2, 5, 10, 3, 5, 2, 3, 4, 5, 3, 8, 4, -1, -1, -1, -1},
            {5, 10, 2, 5, 2, 4, 4, 2, 0, -1, -1, -1, -1, -1, -1, -1},
            {3, 10, 2, 3, 5, 10, 3, 8, 5, 4, 5, 8, 0, 1, 9, -1},
            {5, 10, 2, 5, 2, 4, 1, 9, 2, 9, 4, 2, -1, -1, -1, -1},
            {8, 4, 5, 8, 5, 3, 3, 5, 1, -1, -1, -1, -1, -1, -1, -1},
            {0, 4, 5, 1, 0, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {8, 4, 5, 8, 5, 3, 9, 0, 5, 0, 3, 5, -1, -1, -1, -1},
            {9, 4, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {4, 11, 7, 4, 9, 11, 9, 10, 11, -1, -1, -1, -1, -1, -1, -1},
            {0, 8, 3, 4, 9, 7, 9, 11, 7, 9, 10, 11, -1, -1, -1, -1},
            {1, 10, 11, 1, 11, 4, 1, 4, 0, 7, 4, 11, -1, -1, -1, -1},
            {3, 1, 4, 3, 4, 8, 1, 10, 4, 7, 4, 11, 10, 11, 4, -1},
            {4, 11, 7, 9, 11, 4, 9, 2, 11, 9, 1, 2, -1, -1, -1, -1},
            {9, 7, 4, 9, 11, 7, 9, 1, 11, 2, 11, 1, 0, 8, 3, -1},
            {11, 7, 4, 11, 4, 2, 2, 4, 0, -1, -1, -1, -1, -1, -1, -1},
            {11, 7, 4, 11, 4, 2, 8, 3, 4, 3, 2, 4, -1, -1, -1, -1},
            {2, 9, 10, 2, 7, 9, 2, 3, 7, 7, 4, 9, -1, -1, -1, -1},
            {9, 10, 7, 9, 7, 4, 10, 2, 7, 8, 7, 0, 2, 0, 7, -1},
            {3, 7, 10, 3, 10, 2, 7, 4, 10, 1, 10, 0, 4, 0, 10, -1},
            {1, 10, 2, 8, 7, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {4, 9, 1, 4, 1, 7, 7, 1, 3, -1, -1, -1, -1, -1, -1, -1},
            {4, 9, 1, 4, 1, 7, 0, 8, 1, 8, 7, 1, -1, -1, -1, -1},
            {4, 0, 3, 7, 4, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {4, 8, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {9, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {3, 0, 9, 3, 9, 11, 11, 9, 10, -1, -1, -1, -1, -1, -1, -1},
            {0, 1, 10, 0, 10, 8, 8, 10, 11, -1, -1, -1, -1, -1, -1, -1},
            {3, 1, 10, 11, 3, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {1, 2, 11, 1, 11, 9, 9, 11, 8, -1, -1, -1, -1, -1, -1, -1},
            {3, 0, 9, 3, 9, 11, 1, 2, 9, 2, 11, 9, -1, -1, -1, -1},
            {0, 2, 11, 8, 0, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {3, 2, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {2, 3, 8, 2, 8, 10, 10, 8, 9, -1, -1, -1, -1, -1, -1, -1},
            {9, 10, 2, 0, 9, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {2, 3, 8, 2, 8, 10, 0, 1, 8, 1, 10, 8, -1, -1, -1, -1},
            {1, 10, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {1, 3, 8, 9, 1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {0, 9, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {0, 3, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}};


    public static int[] cube(int[] value){
        int index = 0;
        index+=value[0]*1;
        index+=value[1]*2;
        index+=value[2]*4;
        index+=value[3]*8;
        index+=value[4]*16;
        index+=value[5]*32;
        index+=value[6]*64;
        index+=value[7]*128;
        return triTable[index];
    }
    static int ex = 4;
    public static Point3D zerox(Node n, double y, double z, double xmn, double xmx){
        HashMap<String, Double> vars = new HashMap<String, Double>();
        vars.put("x", xmn);
        vars.put("y", y);
        vars.put("z", z);
        double d = 1;
        double cx = xmn;
        try {
            d = Math.abs(n.getValue(vars));
        }catch(Exception e){
            //nada
        }
        for(double x = xmn;x<=xmx;x+=(xmx-xmn)/ex){
            double c = 1;
            vars.put("x", x);
            try {
                c = Math.abs(n.getValue(vars));
            }catch(Exception e){
                //nada
            }
            if(c<d){
                d=c;
                cx = x;
            }
        }
        return new Point3D(cx,y,z,Color.RED);
    }
    public static Point3D zeroy(Node n, double x, double z, double ymn, double ymx){
        HashMap<String, Double> vars = new HashMap<String, Double>();
        vars.put("x", x);
        vars.put("y", ymn);
        vars.put("z", z);
        double d = 1;
        double cy = ymn;
        try {
            d = Math.abs(n.getValue(vars));
        }catch(Exception e){
            //nada
        }
        for(double y = ymn;y<=ymx;y+=(ymx-ymn)/ex){
            vars.put("y", y);
            double c = 1;
            try {
                c = Math.abs(n.getValue(vars));
            }catch(Exception e){
                //nada
            }
            if(c<d){
                d=c;
                cy = y;
            }
        }
        return new Point3D(x,cy,z,Color.RED);
    }
    public static Point3D zeroz(Node n, double x, double y, double zmn, double zmx){
        HashMap<String, Double> vars = new HashMap<String, Double>();
        vars.put("x", x);
        vars.put("y", y);
        vars.put("z", zmn);
        double d = 1;
        double cz = zmn;
        try {
            d = Math.abs(n.getValue(vars));
        }catch(Exception e){
            //nada
        }
        for(double z = zmn;z<=zmx;z+=(zmx-zmn)/ex){
            vars.put("z", z);
            double c = 1;
            try {
                c = Math.abs(n.getValue(vars));
            }catch(Exception e){
                //nada
            }
            if(c<d){
                d=c;
                cz = z;
            }
        }
        return new Point3D(x,y,cz,Color.RED);
    }
    public static Shape generatei(Node n, double xmn, double xmx, double ymn, double ymx, double zmn, double zmx, double step, Context con){
        //Point3D[] pos = new Point3D[count];

        ((GraphActivity)con).updateProg(0);
        int xl = 0;
        int yl = 0;
        int zl = 0;
        for(double x = xmn;x<=xmx;x+=step){
            xl++;
        }
        for(double y = ymn;y<=ymx;y+=step){
            yl++;
        }
        for(double z = zmn;z<=zmx;z+=step){
            zl++;
        }
        HashMap<String, Double> vars = new HashMap<String, Double>();
        int xp = 0;
        int[][][] cache = new int[xl][yl][zl];
        for(double x = xmn;x<=xmx;x+=step){
            int yp = 0;
            for(double y = ymn;y<=ymx;y+=step){
                int zp = 0;
                for(double z = zmn;z<=zmx;z+=step){
                    vars.put("x", x);
                    vars.put("y", y);
                    vars.put("z", z);
                    try {
                        if (n.getValue(vars) <= 0){
                            cache[xp][yp][zp]=1;
                        }else{
                            cache[xp][yp][zp]=0;
                        }
                    }catch(Exception e){

                    }
                    zp++;
                }
                yp++;
            }
            xp++;
        }
        ((GraphActivity)con).updateProg(10);
        ArrayList<Point3D> par = new ArrayList<Point3D>();
        HashMap<Point3D, Integer> pts = new HashMap<Point3D, Integer>();
        int index = 0;
        double xv = xmn;
        for(int x = 0; x<xl-1;x++) {
            double yv = ymn;
            for (int y = 0; y < yl - 1; y++) {
                double zv = zmn;
                for (int z = 0; z < zl - 1; z++) {
                    int[] verts = new int[8];
                    verts[0]=cache[x][y][z];
                    verts[3]=cache[x][y+1][z];
                    verts[4]=cache[x][y][z+1];
                    verts[7]=cache[x][y+1][z+1];
                    verts[1]=cache[x+1][y][z];
                    verts[2]=cache[x+1][y+1][z];
                    verts[5]=cache[x+1][y][z+1];
                    verts[6]=cache[x+1][y+1][z+1];
                    if(verts[0]!=verts[3]){
                        Point3D p = zeroy(n,xv,zv,yv,yv+step);
                        //Point3D p = new Point3D(xv,(2*yv+step)/2,zv,Color.RED);
                        if(!pts.containsKey(p)){
                            pts.put(p,index);
                            par.add(index,p);
                            index++;
                        }
                        //e0
                    }
                    if(verts[0]!=verts[4]){
                        Point3D p = zeroz(n, xv, yv, zv, zv + step);
                        //Point3D p = new Point3D(xv,yv,(2*zv+step)/2,Color.RED);
                        if(!pts.containsKey(p)){
                            pts.put(p,index);
                            par.add(index,p);
                            index++;
                        }
                        //e1
                    }
                    if(verts[0]!=verts[1]){
                        Point3D p = zerox(n, yv, zv, xv, xv + step);
                        //Point3D p = new Point3D((2*xv+step)/2,yv,zv,Color.RED);
                        if(!pts.containsKey(p)){
                            pts.put(p,index);
                            par.add(index,p);
                            index++;
                        }
                        //e2
                    }
                    if(verts[3]!=verts[7]){
                        Point3D p = zeroz(n, xv, yv+step, zv, zv + step);
                        //Point3D p = new Point3D(xv,yv+step,(2*zv+step)/2,Color.RED);
                        if(!pts.containsKey(p)){
                            pts.put(p,index);
                            par.add(index,p);
                            index++;
                        }
                        //e3
                    }
                    if(verts[3]!=verts[2]){
                        Point3D p = zerox(n, yv+step, zv, xv, xv + step);
                        //Point3D p = new Point3D((2*xv+step)/2,yv+step,zv,Color.RED);
                        if(!pts.containsKey(p)){
                            pts.put(p,index);
                            par.add(index,p);
                            index++;
                        }
                        //e4
                    }
                    if(verts[4]!=verts[7]){
                        Point3D p = zeroy(n, xv, zv + step, yv, yv + step);
                        //Point3D p = new Point3D(xv,(2*yv+step)/2,zv+step,Color.RED);
                        if(!pts.containsKey(p)){
                            pts.put(p,index);
                            par.add(index,p);
                            index++;
                        }
                        //e5
                    }
                    if(verts[4]!=verts[5]){
                        Point3D p = zerox(n, yv, zv + step, xv, xv + step);
                        //Point3D p = new Point3D((2*xv+step)/2,yv,zv+step,Color.RED);
                        if(!pts.containsKey(p)){
                            pts.put(p,index);
                            par.add(index,p);
                            index++;
                        }
                        //e6
                    }
                    if(verts[7]!=verts[6]){
                        Point3D p = zerox(n, yv + step, zv + step, xv, xv + step);
                        //Point3D p = new Point3D((2*xv+step)/2,yv+step,zv+step,Color.RED);
                        if(!pts.containsKey(p)){
                            pts.put(p,index);
                            par.add(index,p);
                            index++;
                        }
                        //e7
                    }
                    if(verts[1]!=verts[2]){
                        Point3D p = zeroy(n,xv+step,zv,yv,yv+step);
                        //Point3D p = new Point3D(xv+step,(2*yv+step)/2,zv,Color.RED);
                        if(!pts.containsKey(p)){
                            pts.put(p,index);
                            par.add(index,p);
                            index++;
                        }
                        //e8
                    }
                    if(verts[1]!=verts[5]){
                        Point3D p = zeroz(n, xv + step, yv, zv, zv + step);
                        //Point3D p = new Point3D(xv+step,yv,(2*zv+step)/2,Color.RED);
                        if(!pts.containsKey(p)){
                            pts.put(p,index);
                            par.add(index,p);
                            index++;
                        }
                        //e9
                    }
                    if(verts[2]!=verts[6]){
                        Point3D p = zeroz(n, xv + step, yv + step, zv, zv + step);
                        //Point3D p = new Point3D(xv+step,yv+step,(2*zv+step)/2,Color.RED);
                        if(!pts.containsKey(p)){
                            pts.put(p,index);
                            par.add(index,p);
                            index++;
                        }
                        //e10
                    }
                    if(verts[5]!=verts[6]){
                        Point3D p = zeroy(n,xv+step,zv+step,yv,yv+step);
                        //Point3D p = new Point3D(xv+step,(2*yv+step)/2,zv+step,Color.RED);
                        if(!pts.containsKey(p)){
                            pts.put(p,index);
                            par.add(index,p);
                            index++;
                        }
                        //e11
                    }
                    zv+=step;
                }
                yv+=step;
            }
            xv+=step;
        }
        ((GraphActivity)con).updateProg(20);
        Point3D[] pos = par.toArray(new Point3D[par.size()]);

        ArrayList<Connector> connectors = new ArrayList<Connector>();
        xv = xmn;
        for(int x = 0; x<xl-1;x++) {
            ((GraphActivity)con).updateProg(80*x/xl+20);
            double yv = ymn;
            for (int y = 0; y < yl - 1; y++) {
                double zv = zmn;
                for (int z = 0; z < zl - 1; z++) {
                    int[] verts = new int[8];
                    verts[0]=cache[x][y][z];
                    verts[3]=cache[x][y+1][z];
                    verts[4]=cache[x][y][z+1];
                    verts[7]=cache[x][y+1][z+1];
                    verts[1]=cache[x+1][y][z];
                    verts[2]=cache[x+1][y+1][z];
                    verts[5]=cache[x+1][y][z+1];
                    verts[6]=cache[x+1][y+1][z+1];
                    Point3D[] edges = new Point3D[12];
                    /*edges[3]= new Point3D(xv,(2*yv+step)/2,zv,Color.RED);
                    edges[8]= new Point3D(xv,yv,(2*zv+step)/2,Color.RED);
                    edges[0]= new Point3D((2*xv+step)/2,yv,zv,Color.RED);
                    edges[11]= new Point3D(xv,yv+step,(2*zv+step)/2,Color.RED);
                    edges[2]= new Point3D((2*xv+step)/2,yv+step,zv,Color.RED);
                    edges[7]= new Point3D(xv,(2*yv+step)/2,zv+step,Color.RED);
                    edges[4]= new Point3D((2*xv+step)/2,yv,zv+step,Color.RED);
                    edges[6]= new Point3D((2*xv+step)/2,yv+step,zv+step,Color.RED);
                    edges[1]= new Point3D(xv+step,(2*yv+step)/2,zv,Color.RED);
                    edges[9]= new Point3D(xv+step,yv,(2*zv+step)/2,Color.RED);
                    edges[10]= new Point3D(xv+step,yv+step,(2*zv+step)/2,Color.RED);
                    edges[5]= new Point3D(xv+step,(2*yv+step)/2,zv+step,Color.RED);*/
                    edges[3]= zeroy(n,xv,zv,yv,yv+step);//new Point3D(xv,(2*yv+step)/2,zv,Color.RED);
                    edges[8]= zeroz(n, xv, yv, zv, zv + step);//new Point3D(xv,yv,(2*zv+step)/2,Color.RED);
                    edges[0]= zerox(n, yv, zv, xv, xv + step);//new Point3D((2*xv+step)/2,yv,zv,Color.RED);
                    edges[11]= zeroz(n, xv, yv+step, zv, zv + step);//new Point3D(xv,yv+step,(2*zv+step)/2,Color.RED);
                    edges[2]= zerox(n, yv+step, zv, xv, xv + step);//new Point3D((2*xv+step)/2,yv+step,zv,Color.RED);
                    edges[7]= zeroy(n, xv, zv + step, yv, yv + step);//new Point3D(xv,(2*yv+step)/2,zv+step,Color.RED);
                    edges[4]= zerox(n, yv, zv + step, xv, xv + step);//new Point3D((2*xv+step)/2,yv,zv+step,Color.RED);
                    edges[6]= zerox(n, yv + step, zv + step, xv, xv + step);//new Point3D((2*xv+step)/2,yv+step,zv+step,Color.RED);
                    edges[1]= zeroy(n,xv+step,zv,yv,yv+step);//new Point3D(xv+step,(2*yv+step)/2,zv,Color.RED);
                    edges[9]= zeroz(n, xv + step, yv, zv, zv + step);//new Point3D(xv+step,yv,(2*zv+step)/2,Color.RED);
                    edges[10]= zeroz(n, xv + step, yv + step, zv, zv + step);//new Point3D(xv+step,yv+step,(2*zv+step)/2,Color.RED);
                    edges[5]= zeroy(n,xv+step,zv+step,yv,yv+step);//new Point3D(xv+step,(2*yv+step)/2,zv+step,Color.RED);
                    int[] tri = cube(verts);
                    for(int i = 0;i<tri.length;i+=3){
                        if(tri[i]>=0) {
                            int i1 = pts.get(edges[tri[i]]);
                            int i2 = pts.get(edges[tri[i+1]]);
                            int i3 = pts.get(edges[tri[i+2]]);
                            connectors.add(new Connector(i1, i2, i3));
                        }
                    }
                    zv+=step;
                }
                yv+=step;
            }
            xv+=step;
        }
        Connector[] cont = connectors.toArray(new Connector[connectors.size()]);
        ((GraphActivity)con).updateProg(10);



        /*int cp = 0;
        for(int x = 1; x<xl-1;x++){
            for(int y = 1; y<yl-1;y++) {
                for (int z = 1; z < zl-1; z++) {
                    int p0 = cache[x][y][z];
                    int p1 = cache[x + 1][y][z];
                    int p2 = cache[x][y + 1][z];
                    int p3 = cache[x][y][z + 1];
                    int p4 = cache[x - 1][y][z];
                    int p5 = cache[x][y - 1][z];
                    int p6 = cache[x][y][z - 1];
                    if (p0==0&&(p0 != p1 || p0 != p2 || p0 != p3 || p0 != p4 || p0 != p5 || p0 != p6)) {
                        cp++;
                    }
                }
            }
        }
        Point3D[] pos = new Point3D[cp];
        double xr = xmn;
        int cnt = 0;
        for(int x = 1; x<xl-1;x++){
            double yr = ymn;
            for(int y = 1; y<yl-1;y++){
                double zr = zmn;
                for(int z = 1; z<zl-1;z++){
                    int[] vert = new int[8];
                    int p0 = cache[x][y][z];
                    int p1 = cache[x+1][y][z];
                    int p2 = cache[x][y+1][z];
                    int p3 = cache[x][y][z+1];
                    int p4 = cache[x-1][y][z];
                    int p5 = cache[x][y-1][z];
                    int p6 = cache[x][y][z-1];
                    if(p0==0&&(p0!=p1||p0!=p2||p0!=p3||p0!=p4||p0!=p5||p0!=p6)){
                        pos[cnt]=new Point3D(xr,yr,zr,Color.RED);
                        cnt++;
                    }
                    zr+=step;
                }
                yr+=step;
            }
            xr+=step;
        }*/
        /*for(int x = 0; x<xl;x++){
            for(int y = 0; y<yl;y++){
                for(int z = 0; z<zl;z++){
//                 v0_______e4_____________v4
//                  /|                    /|
//                 / |                   / |
//              e0/  |                e8/  |
//               /___|______e5_________/   |
//            v1|    |                 |v5 |e9
//              |    |                 |   |
//              |    |e1              |e10 |
//            e2|    |                 |   |
//              |    |_________________|___|
//              |   / v2      e6       |   /v6
//              |  /                   |  /
//              | /e3                  | /e11
//              |/_____________________|/
//              v3         e7          v7
                    int p0 = cache[x][y][z];
                    int p1 = cache[x][y+1][z];
                    int p2 = cache[x][y][z+1];
                    int p3 = cache[x][y+1][z+1];
                    int p4 = cache[x+1][y][z];
                    int p5 = cache[x+1][y+1][z];
                    int p6 = cache[x+1][y][z+1];
                    int p7 = cache[x+1][y+1][z+1];
                    int e0 = (p0==p1)?0:1;
                    int e1 = (p0==p2)?0:1;
                    int e2 = (p1==p3)?0:1;
                    int e3 = (p2==p3)?0:1;
                    int e4 = (p0==p4)?0:1;
                    int e5 = (p1==p5)?0:1;
                    int e6 = (p2==p6)?0:1;
                    int e7 = (p3==p7)?0:1;
                    int e8 = (p4==p5)?0:1;
                    int e9 = (p4==p6)?0:1;
                    int e10 = (p5==p7)?0:1;
                    int e11 = (p6==p7)?0:1;
                }
            }
        }*/
        return new Shape(pos, cont);
    }



    public static int getPos(int xl, int yl, int xp, int yp){
        int n = ((xp*yl)+yp);
        //System.out.println(xp+", "+yp+" : "+xl+", "+yl+" out: "+n);
        return n;
    }
    public static Shape axes(){
        int min=-100,max=100;
        double step = 5;
        int length = (int)((max-min)/step+1);
        Point3D[] pos1 = new Point3D[length*3];
        Connector[] shapes = new Connector[length*3];
        int counter = 0;
        for(double i = min;i<=max;i+=step){
            pos1[counter]= new Point3D(0,0,i,Color.BLACK);
            shapes[counter] = new Connector(counter);
            counter++;
            pos1[counter]= new Point3D(0,i,0,Color.BLACK);
            shapes[counter] = new Connector(counter);
            counter++;
            pos1[counter]= new Point3D(i,0,0,Color.BLACK);
            shapes[counter] = new Connector(counter);
            counter++;
        }
        return new Shape(pos1, shapes);
    }
}