package com.example.brandon.graphmaster;

import java.util.HashMap;

/**
 * Created by Brandon on 9/8/2016.
 */
public class Power implements Node {
    Node n1, n2;
    public Power(Node n1, Node n2){
        this.n1 = n1;
        this.n2 = n2;
    }
    @Override
    public double getValue(HashMap<String, Double> vars) throws Exception{
        return Math.pow(n1.getValue(vars), n2.getValue(vars));
    }
}
