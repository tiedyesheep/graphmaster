package com.example.brandon.graphmaster;
import android.graphics.Color;

/**
 * Created by Brandon on 9/9/2016.
 */
public class Point3D {
    public double x,y,z;
    public Matrix m;
    public int c;
    public Point3D(double x, double y, double z, int c){
        this.x = x;
        this.y = y;
        this.z = z;
        this.c = c;
        double[][] md = {{z},{y},{x},{1}};
        this.m = new Matrix(md);
    }
    public static double D2(Point3D p1, Point3D p2){
        return Math.pow(p1.x-p2.x,2)+Math.pow(p1.y-p2.y,2)+Math.pow(p1.z-p2.z,2);
    }
    @Override
    public int hashCode(){
        return (int)(x*10000+y*100+z);
    }
    @Override
    public boolean equals(Object obj){
        if(true){
            Point3D c = (Point3D)obj;
            if(c.x==x&&c.y==y&&c.z==z) {
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}
