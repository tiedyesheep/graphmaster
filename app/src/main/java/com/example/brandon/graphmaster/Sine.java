package com.example.brandon.graphmaster;

import java.util.HashMap;

/**
 * Created by Brandon on 9/8/2016.
 */
public class Sine implements Node {
    Node n1;
    public Sine(Node n1){
        this.n1 = n1;
    }
    @Override
    public double getValue(HashMap<String,Double> vars) throws Exception{
        //System.out.println("sin("+n1.getValue(vars)+")");
        return Math.sin(n1.getValue(vars));
    }
}

