package com.example.brandon.graphmaster;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;

public class GraphActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        Screen s = null;
        boolean parametric = getIntent().getBooleanExtra("par", false);
        boolean isometric = getIntent().getBooleanExtra("iso", false);
        if(parametric){
            String fx = getIntent().getStringExtra("fx");
            String fy = getIntent().getStringExtra("fy");
            String fz = getIntent().getStringExtra("fz");
            double xmn = getIntent().getDoubleExtra("umn",-15);
            double xmx = getIntent().getDoubleExtra("umx",15);
            double ymn = getIntent().getDoubleExtra("vmn",-15);
            double ymx = getIntent().getDoubleExtra("vmx",15);
            double step = getIntent().getDoubleExtra("step",3);
            try {
                new GetTask(this).execute("par", translate.nodeFromString(fx), translate.nodeFromString(fy), translate.nodeFromString(fz), xmn, xmx, ymn, ymx, step);
            }catch(Exception e){

            }
        }else if(isometric){
            String function = getIntent().getStringExtra("f");
            double xmn = getIntent().getDoubleExtra("xmn", -15);
            double xmx = getIntent().getDoubleExtra("xmx", 15);
            double ymn = getIntent().getDoubleExtra("ymn", -15);
            double ymx = getIntent().getDoubleExtra("ymx", 15);
            double zmn = getIntent().getDoubleExtra("zmn", -15);
            double zmx = getIntent().getDoubleExtra("zmx", 15);
            double step = getIntent().getDoubleExtra("step", 1);
            try {
                new GetTask(this).execute("imp", translate.nodeFromString(function), xmn, xmx, ymn, ymx, zmn, zmx, step);
            }catch(Exception e){

            }
        }else{
            String function = getIntent().getStringExtra("function");
            double xmn = getIntent().getDoubleExtra("xmn", -15);
            double xmx = getIntent().getDoubleExtra("xmx", 15);
            double ymn = getIntent().getDoubleExtra("ymn", -15);
            double ymx = getIntent().getDoubleExtra("ymx", 15);
            double step = getIntent().getDoubleExtra("step", 3);
            try {
                new GetTask(this).execute("exp", translate.nodeFromString(function), xmn, xmx, ymn, ymx, step);
            }catch(Exception e){

            }
        }
    }
    public void init(Shape sh){
        Screen s = null;
        boolean parametric = getIntent().getBooleanExtra("par", false);
        boolean isometric = getIntent().getBooleanExtra("iso", false);
        if(parametric){
            boolean points = getIntent().getBooleanExtra("points", false);
            boolean lines = getIntent().getBooleanExtra("lines", false);
            s=new Screen(this,sh);
            s.points=points;
            s.lines = lines;
            s.invalidate();
            RelativeLayout rl = (RelativeLayout)findViewById(R.id.holder);
            rl.addView(s);
        }else if(isometric){
            boolean points = getIntent().getBooleanExtra("points", false);
            boolean lines = getIntent().getBooleanExtra("lines", false);
            s=new Screen(this,sh);
            s.points = points;
            s.lines = lines;
            s.invalidate();
            RelativeLayout rl = (RelativeLayout) findViewById(R.id.holder);
            rl.addView(s);
        }else{
            boolean points = getIntent().getBooleanExtra("points", false);
            boolean lines = getIntent().getBooleanExtra("lines", false);
            s=new Screen(this,sh);
            s.points = points;
            s.lines = lines;
            s.invalidate();
            RelativeLayout rl = (RelativeLayout) findViewById(R.id.holder);
            rl.addView(s);
        }
    }
    ProgressDialog mDialog;
    class GetTask extends AsyncTask<Object, Void, Shape> {
        Context context;

        GetTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mDialog = new ProgressDialog(context);
            mDialog.setMessage("Generating Shape...");
            mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mDialog.setMax(100);
            mDialog.show();
        }

        @Override
        protected Shape doInBackground(Object... params) {
            if(((String)params[0]).equals("exp")) {
                return Shape.generate((Node)params[1],(double)params[2],(double)params[3],(double)params[4],(double)params[5],(double)params[6], context);
            }else if(((String)params[0]).equals("par")){
                return Shape.generatep((Node)params[1],(Node)params[2],(Node)params[3],(double)params[4],(double)params[5],(double)params[6],(double)params[7],(double)params[8], context);
            }else{
                return Shape.generatei((Node)params[1],(double)params[2],(double)params[3],(double)params[4],(double)params[5],(double)params[6],(double)params[7],(double)params[8], context);
            }
        }

        @Override
        protected void onPostExecute(Shape result) {
            super.onPostExecute(result);
            init(result);
            mDialog.dismiss();
        }
    }
    public void updateProg(int amount){
        mDialog.setProgress(amount);
    }
}
