package com.example.brandon.graphmaster;


/**
 * Created by Brandon on 9/10/2016.
 */
public class ControlPanel /*extends JPanel implements ActionListener*/{
    /*Screen screen;
    JTextField formula;
    JFormattedTextField xmin,xmax,ymin,ymax,step;
    JCheckBox derivitave, points;
    public ControlPanel(Screen screen){
        this.screen = screen;
        JTextArea form = new JTextArea("Formula:");
        formula = new JTextField();
        formula.setText("(x^2-y^2)/5");
        formula.addActionListener(this);

        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.getDefault());
        DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
        decimalFormat.setGroupingUsed(false);

        JTextArea xmn = new JTextArea("X-Min:");
        xmin = new JFormattedTextField(decimalFormat);
        xmin.setColumns(3); //whatever size you wish to set
        xmin.setText("-15");
        xmin.addActionListener(this);

        JTextArea xmx = new JTextArea("X-Max:");
        xmax = new JFormattedTextField(decimalFormat);
        xmax.setColumns(3);
        xmax.setText("15");
        xmax.addActionListener(this);

        JTextArea ymn = new JTextArea("Y-Min:");
        ymin = new JFormattedTextField(decimalFormat);
        ymin.setColumns(3);
        ymin.setText("-15");
        ymin.addActionListener(this);

        JTextArea ymx = new JTextArea("Y-Max:");
        ymax = new JFormattedTextField(decimalFormat);
        xmin.setColumns(3);
        ymax.setText("15");
        ymax.addActionListener(this);

        JTextArea stp = new JTextArea("Step:");
        step = new JFormattedTextField(decimalFormat);
        xmin.setColumns(5);
        step.setText("0.5");
        step.addActionListener(this);

        JTextArea der = new JTextArea("Derivitave:");
        derivitave = new JCheckBox();
        derivitave.addActionListener(this);

        JTextArea pts = new JTextArea("Points:");
        points = new JCheckBox();
        points.addActionListener(this);
        this.setLayout(new GridLayout(2,8));
        this.add(form);
        this.add(formula);

        this.add(xmn);
        this.add(xmin);

        this.add(xmx);
        this.add(xmax);

        this.add(ymn);
        this.add(ymin);

        this.add(ymx);
        this.add(ymax);

        this.add(stp);
        this.add(step);

        this.add(der);
        this.add(derivitave);

        this.add(pts);
        this.add(points);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String form = formula.getText();
        double xmn = Double.parseDouble(xmin.getText());
        double xmx = Double.parseDouble(xmax.getText());
        double ymn = Double.parseDouble(ymin.getText());
        double ymx = Double.parseDouble(ymax.getText());
        double stp = Double.parseDouble(step.getText());
        boolean der = derivitave.isSelected();
        boolean pts = points.isSelected();
        if(der){
            screen.shape = Shape.generate(translate.differentiateNode(translate.nodeFromString(form)), xmn, xmx, ymn, ymx, stp);
        }else {
            screen.shape = Shape.generate(translate.nodeFromString(form), xmn, xmx, ymn, ymx, stp);
        }
        screen.points = pts;
        screen.init(screen.shape);
        screen.invalidate();
        screen.revalidate();
        screen.repaint();
    }*/
}
