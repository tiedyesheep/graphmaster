package com.example.brandon.graphmaster;

/**
 * Created by Brandon on 11/16/2016.
 */
public class PFunction {
    String name;
    String fx,fy,fz;
    Double umn,umx,vmn,vmx,step;
    public PFunction(String name, String fx, String fy, String fz, double umn, double umx, double vmn, double vmx, double step){
        this.name = name;
        this.fx = fx;
        this.fy = fy;
        this.fz = fz;
        this.umn = umn;
        this.umx = umx;
        this.vmn = vmn;
        this.vmx = vmx;
        this.step = step;
    }
}
