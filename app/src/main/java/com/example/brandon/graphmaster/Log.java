package com.example.brandon.graphmaster;

import java.util.HashMap;

/**
 * Created by Brandon on 9/9/2016.
 */
public class Log implements Node {
    Node n1;
    public Log(Node n1){
        this.n1 = n1;
    }
    @Override
    public double getValue(HashMap<String, Double> vars) throws Exception{
        return Math.log10(n1.getValue(vars));
    }
}
