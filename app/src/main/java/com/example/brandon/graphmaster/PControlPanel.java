package com.example.brandon.graphmaster;


/**
 * Created by Brandon on 9/10/2016.
 */
public class PControlPanel /*extends JPanel implements ActionListener*/{
    /*Screen screen;
    JTextField formulax;
    JTextField formulay;
    JTextField formulaz;
    JFormattedTextField xmin,xmax,ymin,ymax,step;
    JCheckBox derivitave, points;
    public PControlPanel(Screen screen){
        this.screen = screen;
        JTextArea formx = new JTextArea("X Formula:");
        formulax = new JTextField();
        formulax.setText("sin(u)");
        formulax.addActionListener(this);

        JTextArea formy = new JTextArea("Y Formula:");
        formulay = new JTextField();
        formulay.setText("u");
        formulay.addActionListener(this);

        JTextArea formz = new JTextArea("Z Formula:");
        formulaz = new JTextField();
        formulaz.setText("cos(u)");
        formulaz.addActionListener(this);

        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.getDefault());
        DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
        decimalFormat.setGroupingUsed(false);

        JTextArea xmn = new JTextArea("U-Min:");
        xmin = new JFormattedTextField(decimalFormat);
        xmin.setColumns(3); //whatever size you wish to set
        xmin.setText("-15");
        xmin.addActionListener(this);

        JTextArea xmx = new JTextArea("U-Max:");
        xmax = new JFormattedTextField(decimalFormat);
        xmax.setColumns(3);
        xmax.setText("15");
        xmax.addActionListener(this);

        JTextArea ymn = new JTextArea("V-Min:");
        ymin = new JFormattedTextField(decimalFormat);
        ymin.setColumns(3);
        ymin.setText("-15");
        ymin.addActionListener(this);

        JTextArea ymx = new JTextArea("V-Max:");
        ymax = new JFormattedTextField(decimalFormat);
        xmin.setColumns(3);
        ymax.setText("15");
        ymax.addActionListener(this);

        JTextArea stp = new JTextArea("Step:");
        step = new JFormattedTextField(decimalFormat);
        xmin.setColumns(5);
        step.setText("0.5");
        step.addActionListener(this);

        JTextArea der = new JTextArea("Derivitave:");
        derivitave = new JCheckBox();
        derivitave.addActionListener(this);

        JTextArea pts = new JTextArea("Points:");
        points = new JCheckBox();
        points.addActionListener(this);
        this.setLayout(new GridLayout(3,6));
        this.add(formx);
        this.add(formulax);

        this.add(formy);
        this.add(formulay);

        this.add(formz);
        this.add(formulaz);

        this.add(xmn);
        this.add(xmin);

        this.add(xmx);
        this.add(xmax);

        this.add(ymn);
        this.add(ymin);

        this.add(ymx);
        this.add(ymax);

        this.add(stp);
        this.add(step);

        this.add(der);
        this.add(derivitave);

        this.add(pts);
        this.add(points);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String formx = formulax.getText();
        String formy = formulay.getText();
        String formz = formulaz.getText();
        double xmn = Double.parseDouble(xmin.getText());
        double xmx = Double.parseDouble(xmax.getText());
        double ymn = Double.parseDouble(ymin.getText());
        double ymx = Double.parseDouble(ymax.getText());
        double stp = Double.parseDouble(step.getText());
        boolean der = derivitave.isSelected();
        boolean pts = points.isSelected();
        if(der){
            Node x = translate.differentiateNode(translate.nodeFromString(formx));
            Node y = translate.differentiateNode(translate.nodeFromString(formy));
            Node z = translate.differentiateNode(translate.nodeFromString(formz));
            screen.shape = Shape.generatep(x,y,z, xmn, xmx, ymn, ymx, stp);
        }else {
            Node x = translate.nodeFromString(formx);
            Node y = translate.nodeFromString(formy);
            Node z = translate.nodeFromString(formz);
            screen.shape = Shape.generatep(x,y,z, xmn, xmx, ymn, ymx, stp);
        }
        screen.points = pts;
        screen.init(screen.shape);
        screen.invalidate();
        screen.revalidate();
        screen.repaint();
    }*/
}
