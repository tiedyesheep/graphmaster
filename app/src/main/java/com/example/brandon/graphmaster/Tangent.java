package com.example.brandon.graphmaster;

import java.util.HashMap;

/**
 * Created by Brandon on 9/8/2016.
 */
public class Tangent implements Node {
    Node n1;
    public Tangent(Node n1){
        this.n1 = n1;
    }
    @Override
    public double getValue(HashMap<String,Double> vars) throws Exception{
        //System.out.println("tan("+n1.getValue(vars)+")");
        return Math.tan(n1.getValue(vars));
    }
}
