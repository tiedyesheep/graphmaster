package com.example.brandon.graphmaster;

/**
 * Created by Brandon on 9/10/2016.
 */
public class Connector {
    int p1,p2,p3;
    boolean axis = false;
    public Connector(int p1, int p2, int p3){
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }
    public Connector(int p1){
        axis = true;
        this.p1 = p1;
    }
}
