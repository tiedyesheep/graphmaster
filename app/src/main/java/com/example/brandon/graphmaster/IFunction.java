package com.example.brandon.graphmaster;

/**
 * Created by Brandon on 11/16/2016.
 */
public class IFunction {
    String name;
    String f;
    Double xmn,xmx,ymn,ymx,zmn,zmx,step;
    public IFunction(String name, String f, double xmn, double xmx, double ymn, double ymx, double zmn, double zmx, double step){
        this.name = name;
        this.f = f;
        this.xmn = xmn;
        this.xmx = xmx;
        this.ymn = ymn;
        this.ymx = ymx;
        this.zmn = zmn;
        this.zmx = zmx;
        this.step = step;
    }
}
