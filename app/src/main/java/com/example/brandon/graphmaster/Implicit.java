package com.example.brandon.graphmaster;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class Implicit extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_implicit);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setTitleTextColor(Color.WHITE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.explicit, menu);
        MenuItem item = menu.findItem(R.id.spinner);
        Spinner spinner = (Spinner) MenuItemCompat.getActionView(item);
        ArrayList<IFunction> functions = new ArrayList<IFunction>();
        try {
            functions.add(new IFunction("Pipe", "cos(x/5)+cos(y/5)+cos(z/5)", -20, 20, -20, 20, -20, 20, 3));
            functions.add(new IFunction("Tangle", "(x/7)^4-5*(x/7)^2+(y/7)^4-5*(y/7)^2+(z/7)^4-5*(z/7)^2+11.8", -20, 20, -20, 20, -20, 20, 3));
            functions.add(new IFunction("Heart", "((2(x/10)^2+(y/10)^2+(z/10)^2-1)^3)-((x/10)^2*(z/10)^3/10)-((y/10)^2*(z/10)^3)", -20, 20, -20, 20, -20, 20, 3));
            functions.add(new IFunction("Hyperboloid 1", "x^2+y^2-z^2-10", -20,20,-20,20,-20,20,3));
            functions.add(new IFunction("Hyperboloid 2", "x^2+y^2-z^2+10", -20,20,-20,20,-20,20,3));
            functions.add(new IFunction("Klein", "((x/5)^2+(y/5)^2+(z/5)^2+2*(y/5)-1)*(((x/5)^2+(y/5)^2+(z/5)^2-2*(y/5)-1)^2-8*(z/5)^2)+16*x/5*z/5*((x/5)^2+(y/5)^2+(z/5)^2-2*y/5-1)", -20,20,-20,20,-20,20,2.5));
            functions.add(new IFunction("Sinus", "sin(pi*((x/20)^2+(y/20)^2))/2+z/20", -20,20,-20,20,-20,20,3));
            functions.add(new IFunction("Octahedron", "abs(x)+abs(y)+abs(z)-20", -20,20,-20,20,-20,20,3));
            functions.add(new IFunction("Cube", "(x/10)^100+(y/10)^100+(z/10)^100-1", -11,11,-11,11,-11,11,2));
            functions.add(new IFunction("Toupie", "(((x*x+y*y)/16)^0.5-3)^3+z*z/16-1", -20,20,-20,20,-20,20,2));
            functions.add(new IFunction("Round Cube", "(x^4+y^4+z^4)/256-(x^2+y^2+z^2)", -20,20,-20,20,-20,20,3));
            //functions.add(new IFunction("Prison", "((x/15)^8+(z/15)^30+(y/15)^8-((x/15)^4+(z/15)^50+(y/15)^4-0.3))*((x/15)^2+(y/15)^2+(z/15)^2-0.5)", -20,20,-20,20,-20,20,3));
            functions.add(new IFunction("Holes", "3*(cos(x/6)+cos(y/6)+cos(z/6))+4*cos(x/6)*cos(y/6)*cos(z/6)", -20,20,-20,20,-20,20,3));
            functions.add(new IFunction("Diamond", "sin(x/5)*sin(y/5)*sin(z/5)+sin(x/5)*cos(y/5)*cos(z/5)+cos(x/5)*sin(y/5)*cos(z/5)+cos(x/5)*cos(y/5)*sin(z/5)", -20,20,-20,20,-20,20,2.5));
            functions.add(new IFunction("Tetrahedral", "((x/5)^2+(y/5)^2+(z/5)^2)^2+8*x*y*z/125-10*((x/5)^2+(y/5)^2+(z/5)^2)+25", -20,20,-20,20,-20,20,2.5));
            functions.add(new IFunction("Virus", "-(x^2+y^2+z^2)/625+cos(5*x/25)*cos(5*y/25)*cos(5*z/25)+0.215", -20,20,-20,20,-20,20,2.5));
        }catch(Exception e){

        }
        ImplicitAdapter adapter = new ImplicitAdapter(this, functions);
        spinner.setAdapter(adapter); // set the adapter to provide layout of rows and content
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                set((IFunction) view.getTag());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return true;
    }


    public void set(IFunction f){
        EditText funct = (EditText)findViewById(R.id.f);
        funct.setText(f.f);
        EditText xmax = (EditText)findViewById(R.id.xmx);
        xmax.setText(Double.toString(f.xmx));
        EditText xmin = (EditText)findViewById(R.id.xmn);
        xmin.setText(Double.toString(f.xmn));
        EditText ymax = (EditText)findViewById(R.id.ymx);
        ymax.setText(Double.toString(f.ymx));
        EditText ymin = (EditText)findViewById(R.id.ymn);
        ymin.setText(Double.toString(f.ymn));
        EditText zmax = (EditText)findViewById(R.id.zmx);
        zmax.setText(Double.toString(f.zmx));
        EditText zmin = (EditText)findViewById(R.id.zmn);
        zmin.setText(Double.toString(f.zmn));
        EditText stp = (EditText)findViewById(R.id.step);
        stp.setText(Double.toString(f.step));
    }
    public void graph(View v){
        boolean par = false;
        boolean iso = true;
        try {
            EditText funct = (EditText)findViewById(R.id.f);
            String f = funct.getText().toString();
            EditText xmax = (EditText)findViewById(R.id.xmx);
            double xmx = Double.parseDouble(xmax.getText().toString());
            EditText xmin = (EditText)findViewById(R.id.xmn);
            double xmn = Double.parseDouble(xmin.getText().toString());
            EditText ymax = (EditText)findViewById(R.id.ymx);
            double ymx = Double.parseDouble(ymax.getText().toString());
            EditText ymin = (EditText)findViewById(R.id.ymn);
            double ymn = Double.parseDouble(ymin.getText().toString());
            EditText zmax = (EditText)findViewById(R.id.zmx);
            double zmx = Double.parseDouble(zmax.getText().toString());
            EditText zmin = (EditText)findViewById(R.id.zmn);
            double zmn = Double.parseDouble(zmin.getText().toString());
            EditText stp = (EditText)findViewById(R.id.step);
            double step = Double.parseDouble(stp.getText().toString());
            CheckBox dot = (CheckBox) findViewById(R.id.dots);
            boolean points = dot.isChecked();
            //CheckBox deri = (CheckBox) findViewById(R.id.der);
            //boolean der = deri.isChecked();
            CheckBox line = (CheckBox) findViewById(R.id.lines);
            boolean lines = line.isChecked();
            boolean xv=true,yv=true,zv=true;
            HashMap<String, Double> vars = new HashMap<String, Double>();
            vars.put("x",(xmn));
            vars.put("y",(ymn));
            vars.put("z",zmn);
            try {
                translate.nodeFromString(f).getValue(vars);
            }catch(Exception e){
                xv = false;
            }
            /*try{
                Shape.generatei(translate.nodeFromString(f),xmn,xmx,ymn,ymx,zmn,zmx,step);
            }catch(Exception e){
                Toast.makeText(Implicit.this, "Unable to generate shape!", Toast.LENGTH_SHORT).show();
                return;
            }*/
            if (xmn < xmx && ymn < ymx && zmn < zmx) {
                if (xv) {
                    if ((xmx - xmn) / step * (ymx - ymn) / step * (zmx -zmn) / step<100000) {
                        Intent intent = new Intent(this, GraphActivity.class);
                        intent.putExtra("iso", true);
                        intent.putExtra("par", false);
                        intent.putExtra("f", f);
                        intent.putExtra("xmx", xmx);
                        intent.putExtra("xmn", xmn);
                        intent.putExtra("ymx", ymx);
                        intent.putExtra("ymn", ymn);
                        intent.putExtra("zmx", zmx);
                        intent.putExtra("zmn", zmn);
                        intent.putExtra("step", step);
                        intent.putExtra("points", points);
                        intent.putExtra("der", false);
                        intent.putExtra("lines", lines);
                        startActivity(intent);
                    } else {
                        Toast.makeText(Implicit.this, "Domain too large! try a larger step or smaller bounds.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (!xv)
                        Toast.makeText(Implicit.this, "Function Invalid!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(Implicit.this, "Min must be smaller than max!", Toast.LENGTH_SHORT).show();
            }
        }catch(Exception e){
            Toast.makeText(Implicit.this, "Parameters invalid!", Toast.LENGTH_SHORT).show();
        }
    }
    public void back(View v){
        finish();
        //Intent intent = new Intent(this, MainActivity.class);
        //startActivity(intent);
    }
    /*@Override
    protected void onSaveInstanceState(Bundle outBundle){
        EditText functx = (EditText)findViewById(R.id.fx);
        String fx = functx.getText().toString();
        EditText functy = (EditText)findViewById(R.id.fy);
        String fy = functy.getText().toString();
        EditText functz = (EditText)findViewById(R.id.fz);
        String fz = functz.getText().toString();
        EditText xmax = (EditText)findViewById(R.id.umx);
        double umx = Double.parseDouble(xmax.getText().toString());
        EditText xmin = (EditText)findViewById(R.id.umn);
        double umn = Double.parseDouble(xmin.getText().toString());
        EditText ymax = (EditText)findViewById(R.id.vmx);
        double vmx = Double.parseDouble(ymax.getText().toString());
        EditText ymin = (EditText)findViewById(R.id.vmn);
        double vmn = Double.parseDouble(ymin.getText().toString());
        EditText stp = (EditText)findViewById(R.id.step);
        double step = Double.parseDouble(stp.getText().toString());
        CheckBox dot = (CheckBox) findViewById(R.id.dots);
        boolean points = dot.isChecked();
        CheckBox deri = (CheckBox) findViewById(R.id.der);
        boolean der = deri.isChecked();
        CheckBox line = (CheckBox) findViewById(R.id.lines);
        boolean lines = line.isChecked();
        outBundle.putCharSequence("fx", fx);
        outBundle.putCharSequence("fy", fy);
        outBundle.putCharSequence("fz", fz);
        outBundle.putDouble("umx", umx);
        outBundle.putDouble("umn", umn);
        outBundle.putDouble("vmx", vmx);
        outBundle.putDouble("vmn", vmn);
        outBundle.putDouble("step", step);
        outBundle.putBoolean("points", points);
        outBundle.putBoolean("der", der);
        outBundle.putBoolean("lines", lines);
        outBundle.putBoolean("saved", true);
    }*/
}
