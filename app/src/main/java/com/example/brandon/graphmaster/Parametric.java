package com.example.brandon.graphmaster;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class Parametric extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametric);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setTitleTextColor(Color.WHITE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.explicit, menu);
        MenuItem item = menu.findItem(R.id.spinner);
        Spinner spinner = (Spinner) MenuItemCompat.getActionView(item);
        ArrayList<PFunction> functions = new ArrayList<PFunction>();
        try {
            functions.add(new PFunction("Sphere", "sin(u)*cos(v)*20", "sin(u)*sin(v)*20", "cos(u)*20", 0, 3.5, 0, 7, 0.2));
            functions.add(new PFunction("Torus", "(2+sin(u))*cos(v)*20", "(2+sin(u))*sin(v)*20", "cos(u)*20", 0, 7, 0, 7, 0.5));
            functions.add(new PFunction("Shell", "1.2^v*(sin(u)^2*sin(v))*20", "1.2^v*(sin(u)*cos(u))*20", "1.2^v*(sin(u)^2*cos(v))*20", 0, 3.5, -0.5, 8, 0.3));
            functions.add(new PFunction("Mobius Strip","(cos(v)+u*cos(v/2)*cos(v))*20","20*u*sin(v/2)","(sin(v)+u*cos(v/2)*sin(v))*20",-0.5,0.5,0,7,0.1));
            functions.add(new PFunction("Klein 2","10*((2+cos(v/2)*sin(u)-sin(v/2)*sin(2*u))*cos(v))","10*(sin(v/2)*sin(u)+cos(v/2)*sin(2*u))","10*((2+cos(v/2)*sin(u)-sin(v/2)*sin(2*u))*sin(v))",0,7,0,7,0.3));
            functions.add(new PFunction("Spiral","(2+cos(u))*cos(v)*20","(sin(u)+v)*20","(2+cos(u))*sin(v)*20",0,7,-7,7,0.5));
            functions.add(new PFunction("Trumpet","cos(u)*sin(v)*20","(cos(v)+ln(tan(1/2*v)))*20","sin(u)*sin(v)*20",0,7,0.03,2,0.3));
            functions.add(new PFunction("Astroid","cos(v)^3*cos(u)^3*20","sin(u)^3*20","sin(v)^3*cos(u)^3*20",-1.5,1.5,0,7,0.2));
            functions.add(new PFunction("Hyperhelicoidal","(sinh(v)*cos(3*u))/(1+cosh(u)*cosh(v))*20","(cosh(v)*sinh(u))/(1+cosh(u)*cosh(v))*20","(sinh(v)*sin(3*u))/(1+cosh(u)*cosh(v))*20",-3.5,3.5,-3.5,3.5,0.3));
            functions.add(new PFunction("Dini","cos(u)*sin(v)*20","20*((cos(v)+ln(tan(v/2)))+0.2*u)","20*sin(u)*sin(v)",0,12.5,0.1,2.1,0.2));
            functions.add(new PFunction("Folium","20*cos(u)*(2*v/pi-tanh(v))","20*cos(u-2*pi/3)/cosh(v)","20*cos(u+2*pi/3)/cosh(v)",-3.5,3.5,-3.5,3.5,0.3));
        }catch(Exception e){

        }
        ParametricAdapter adapter = new ParametricAdapter(this, functions);
        spinner.setAdapter(adapter); // set the adapter to provide layout of rows and content
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                set((PFunction) view.getTag());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return true;
    }


    public void set(PFunction f){
        EditText functx = (EditText)findViewById(R.id.fx);
        functx.setText(f.fx);
        EditText functy = (EditText)findViewById(R.id.fy);
        functy.setText(f.fy);
        EditText functz = (EditText)findViewById(R.id.fz);
        functz.setText(f.fz);
        EditText umax = (EditText)findViewById(R.id.umx);
        umax.setText(Double.toString(f.umx));
        EditText umin = (EditText)findViewById(R.id.umn);
        umin.setText(Double.toString(f.umn));
        EditText vmax = (EditText)findViewById(R.id.vmx);
        vmax.setText(Double.toString(f.vmx));
        EditText vmin = (EditText)findViewById(R.id.vmn);
        vmin.setText(Double.toString(f.vmn));
        EditText stp = (EditText)findViewById(R.id.step);
        stp.setText(Double.toString(f.step));
    }
    public void graph(View v){
        boolean par = true;
        try {
            EditText functx = (EditText)findViewById(R.id.fx);
            String fx = functx.getText().toString();
            EditText functy = (EditText)findViewById(R.id.fy);
            String fy = functy.getText().toString();
            EditText functz = (EditText)findViewById(R.id.fz);
            String fz = functz.getText().toString();
            EditText xmax = (EditText)findViewById(R.id.umx);
            double umx = Double.parseDouble(xmax.getText().toString());
            EditText xmin = (EditText)findViewById(R.id.umn);
            double umn = Double.parseDouble(xmin.getText().toString());
            EditText ymax = (EditText)findViewById(R.id.vmx);
            double vmx = Double.parseDouble(ymax.getText().toString());
            EditText ymin = (EditText)findViewById(R.id.vmn);
            double vmn = Double.parseDouble(ymin.getText().toString());
            EditText stp = (EditText)findViewById(R.id.step);
            double step = Double.parseDouble(stp.getText().toString());
            CheckBox dot = (CheckBox) findViewById(R.id.dots);
            boolean points = dot.isChecked();
            //CheckBox deri = (CheckBox) findViewById(R.id.der);
            //boolean der = deri.isChecked();
            CheckBox line = (CheckBox) findViewById(R.id.lines);
            boolean lines = line.isChecked();
            boolean xv=true,yv=true,zv=true;
            HashMap<String, Double> vars = new HashMap<String, Double>();
            vars.put("u",(umn));
            vars.put("v",(vmn));
            try {
                translate.nodeFromString(fx).getValue(vars);
            }catch(Exception e){
                xv = false;
            }
            try {
                translate.nodeFromString(fy).getValue(vars);
            }catch(Exception e){
                yv = false;
            }
            try {
                translate.nodeFromString(fz).getValue(vars);
            }catch(Exception e){
                zv = false;
            }
            if (umn < umx && vmn < vmx) {
                if (xv && yv && zv) {
                    if ((umx - umn) / step * (vmx - vmn) / step < 10000) {
                        Intent intent = new Intent(this, GraphActivity.class);
                        intent.putExtra("par", par);
                        intent.putExtra("iso", false);
                        intent.putExtra("fx", fx);
                        intent.putExtra("fy", fy);
                        intent.putExtra("fz", fz);
                        intent.putExtra("umx", umx);
                        intent.putExtra("umn", umn);
                        intent.putExtra("vmx", vmx);
                        intent.putExtra("vmn", vmn);
                        intent.putExtra("step", step);
                        intent.putExtra("points", points);
                        intent.putExtra("der", false);
                        intent.putExtra("lines", lines);
                        startActivity(intent);
                    } else {
                        Toast.makeText(Parametric.this, "Domain too large! try a larger step or smaller bounds.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (!xv)
                        Toast.makeText(Parametric.this, "X Function Invalid!", Toast.LENGTH_SHORT).show();
                    if (!yv)
                        Toast.makeText(Parametric.this, "Y Function Invalid!", Toast.LENGTH_SHORT).show();
                    if (!zv)
                        Toast.makeText(Parametric.this, "Z Function Invalid!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(Parametric.this, "Min must be smaller than max!", Toast.LENGTH_SHORT).show();
            }
        }catch(Exception e){
            Toast.makeText(Parametric.this, "Parameters invalid!", Toast.LENGTH_SHORT).show();
        }
    }
    public void back(View v){
        finish();
        //Intent intent = new Intent(this, MainActivity.class);
        //startActivity(intent);
    }
    /*@Override
    protected void onSaveInstanceState(Bundle outBundle){
        EditText functx = (EditText)findViewById(R.id.fx);
        String fx = functx.getText().toString();
        EditText functy = (EditText)findViewById(R.id.fy);
        String fy = functy.getText().toString();
        EditText functz = (EditText)findViewById(R.id.fz);
        String fz = functz.getText().toString();
        EditText xmax = (EditText)findViewById(R.id.umx);
        double umx = Double.parseDouble(xmax.getText().toString());
        EditText xmin = (EditText)findViewById(R.id.umn);
        double umn = Double.parseDouble(xmin.getText().toString());
        EditText ymax = (EditText)findViewById(R.id.vmx);
        double vmx = Double.parseDouble(ymax.getText().toString());
        EditText ymin = (EditText)findViewById(R.id.vmn);
        double vmn = Double.parseDouble(ymin.getText().toString());
        EditText stp = (EditText)findViewById(R.id.step);
        double step = Double.parseDouble(stp.getText().toString());
        CheckBox dot = (CheckBox) findViewById(R.id.dots);
        boolean points = dot.isChecked();
        CheckBox deri = (CheckBox) findViewById(R.id.der);
        boolean der = deri.isChecked();
        CheckBox line = (CheckBox) findViewById(R.id.lines);
        boolean lines = line.isChecked();
        outBundle.putCharSequence("fx", fx);
        outBundle.putCharSequence("fy", fy);
        outBundle.putCharSequence("fz", fz);
        outBundle.putDouble("umx", umx);
        outBundle.putDouble("umn", umn);
        outBundle.putDouble("vmx", vmx);
        outBundle.putDouble("vmn", vmn);
        outBundle.putDouble("step", step);
        outBundle.putBoolean("points", points);
        outBundle.putBoolean("der", der);
        outBundle.putBoolean("lines", lines);
        outBundle.putBoolean("saved", true);
    }*/
}
