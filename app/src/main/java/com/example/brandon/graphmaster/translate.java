package com.example.brandon.graphmaster;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Brandon on 9/8/2016.
 */
public class translate {
    public static Node differentiateNode(Node n){
        if(n.getClass().equals(Plus.class)){
            Plus a = (Plus)n;
            return new Plus(differentiateNode(a.n1),differentiateNode(a.n2));
        }else if(n.getClass().equals(Minus.class)){
            Minus a = (Minus)n;
            return new Minus(differentiateNode(a.n1),differentiateNode(a.n2));
        }else if(n.getClass().equals(Times.class)){
            Times a = (Times)n;
            return new Plus(new Times(differentiateNode(a.n1),a.n2),new Times(differentiateNode(a.n2),a.n1));
        }else if(n.getClass().equals(Divide.class)){
            Divide a = (Divide)n;
            return new Divide(new Minus(new Times(differentiateNode(a.n1),a.n2), new Times(a.n1,differentiateNode(a.n2))),new Power(a.n2,new Number(2)));
        }else if(n.getClass().equals(Power.class)){
            Power a = (Power)n;
            if(a.n1.getClass().equals(Variable.class)&&a.n2.getClass().equals(Number.class)){
                return new Times(a.n2,new Power(a.n1,new Minus(a.n2,new Number(1))));
            }else {
                if (a.n1.getClass().equals(Variable.class))
                    return new Times(new Power(a.n1, a.n2), new Plus(new Times(differentiateNode(a.n1), new Divide(a.n2, a.n1)), new Times(differentiateNode(a.n2), new NaturalLog(a.n1))));
            }
        }else if(n.getClass().equals(Sine.class)){
            Sine a = (Sine)n;
            return new Times(new Cosine(a.n1),differentiateNode(a.n1));
        }else if(n.getClass().equals(Cosine.class)){
            Cosine a = (Cosine)n;
            return new Times(new Number(-1),new Times(new Sine(a.n1),differentiateNode(a.n1)));
        }else if(n.getClass().equals(Tangent.class)){
            Tangent a = (Tangent)n;
            return new Times(new Plus(new Number(1),new Power(new Tangent(a.n1),new Number(2))),differentiateNode(a.n1));
        }else if(n.getClass().equals(NaturalLog.class)){
            NaturalLog a = (NaturalLog)n;
            return new Divide(differentiateNode(a.n1),a.n1);
        }else if(n.getClass().equals(Log.class)){
            Log a = (Log)n;
            return differentiateNode(new Divide(new NaturalLog(a.n1), new NaturalLog(new Number(10))));
        }else if(n.getClass().equals(Absolute.class)){
            Absolute a = (Absolute)n;
            return new Divide(new Times(a.n1,differentiateNode(a.n1)),new Absolute(a.n1));
        }else if(n.getClass().equals(Number.class)){
            return new Number(0);
        }else if(n.getClass().equals(Variable.class)){
            return new Number(1);
        }
        return new Number(0);
    }
    public static Node nodeFromString(String expression) throws Exception{
        System.out.println();
        ArrayList<Node> nodes = new ArrayList<Node>();
        for(int i = 0;i<expression.length();i++){
            char c = expression.charAt(i);
            if(c=='('){
                //System.out.println("parenteses");
                int pc = 1;
                for(int a = i+1;pc!=0;a++){
                    char ca = expression.charAt(a);
                    if(ca=='('){
                        pc++;
                    }else if(ca==')'){
                        pc--;
                    }
                    //System.out.println(pc);
                    if(pc==0){
                        String sub = expression.substring(i+1,a);
                        //System.out.println("sub:  "+sub);
                        Node n = nodeFromString(sub);
                        nodes.add(n);
                        String node = ";"+(nodes.size()-1)+";";
                        expression = expression.replace("("+sub+")",node);
                        i+=node.length()-1;

                    }
                }
            }else if(Character.isDigit(c)||c=='.'||c=='-'){
                System.out.println("digit");
                System.out.println(expression);
                boolean good = false;
                if(c=='-'){
                    if(i==0){
                        good=true;
                    }else{
                        char p = expression.charAt(i-1);
                        if(p=='*'||p=='+'||p=='/'||p=='('||p=='^'){
                            good = true;
                        }
                    }
                }else{
                    good = true;
                }
                if(good) {
                    int a = i;
                    for (; a < expression.length() && (Character.isDigit(expression.charAt(a)) || expression.charAt(a) == '.'||good); a++)
                        good=false;
                    String q = expression.substring(i, a);
                    if(q.equals("-")){
                        q="-1";
                    }
                    Node n = new Number(Double.parseDouble(q));
                    nodes.add(n);
                    String p1 = expression.substring(0, i);
                    String p2;
                    if (a + 1 > expression.length()) {
                        p2 = "";
                    } else {
                        p2 = expression.substring(a, expression.length());
                    }
                    //System.out.println("sub: "+p2);
                    String node = ";" + (nodes.size() - 1) + ";";
                    expression = p1 + node + p2;
                    i += node.length() - 1;
                    //System.out.println(expression);
                    //System.out.println();
                }
            }else if(c==';'){
                int a = i+1;
                for(;a<expression.length()&&expression.charAt(a)!=';';a++);
                i=a;
            }
            System.out.println(expression);
        }
        System.out.println("1");
        for(int i = 0;i<expression.length();i++){
            char c = expression.charAt(i);
            if(i<expression.length()-1&&c=='p'&&expression.charAt(i+1)=='i'){
                Node n = new Number(Math.PI);
                nodes.add(n);
                String node = ";"+(nodes.size()-1)+";";
                expression = expression.replace("pi",node);
                i+=node.length()-1;
            }else if(i<expression.length()-4&&c=='s'&&expression.charAt(i+1)=='i'&&expression.charAt(i+2)=='n'&&expression.charAt(i+3)=='h'){
                //System.out.println("sinh");
                int nn = nodeNumber(expression,i+5);
                int a = ("inh;"+nn+";").length();
                Node n1 = nodes.get(nn);
                Node n = new HSine(n1);
                nodes.add(n);
                String node = ";"+(nodes.size()-1)+";";
                expression = expression.replace("sinh;"+nn+";",node);
                i+=node.length()-1;
            }else if(i<expression.length()-4&&c=='c'&&expression.charAt(i+1)=='o'&&expression.charAt(i+2)=='s'&&expression.charAt(i+3)=='h'){
                //System.out.println("sinh");
                int nn = nodeNumber(expression,i+5);
                int a = ("osh;"+nn+";").length();
                Node n1 = nodes.get(nn);
                Node n = new HCosine(n1);
                nodes.add(n);
                String node = ";"+(nodes.size()-1)+";";
                expression = expression.replace("cosh;"+nn+";",node);
                i+=node.length()-1;
            }else if(i<expression.length()-4&&c=='t'&&expression.charAt(i+1)=='a'&&expression.charAt(i+2)=='n'&&expression.charAt(i+3)=='h'){
                //System.out.println("sinh");
                int nn = nodeNumber(expression,i+5);
                int a = ("anh;"+nn+";").length();
                Node n1 = nodes.get(nn);
                Node n = new HTangent(n1);
                nodes.add(n);
                String node = ";"+(nodes.size()-1)+";";
                expression = expression.replace("tanh;"+nn+";",node);
                i+=node.length()-1;
            }else if(c=='s'&&expression.charAt(i+1)=='i'&&expression.charAt(i+2)=='n'){
                //System.out.println("sin");
                int nn = nodeNumber(expression,i+4);
                int a = ("in;"+nn+";").length();
                Node n1 = nodes.get(nn);
                Node n = new Sine(n1);
                nodes.add(n);
                String node = ";"+(nodes.size()-1)+";";
                expression = expression.replace("sin;"+nn+";",node);
                i+=node.length()-1;
            }else if(c=='c'&&expression.charAt(i+1)=='o'&&expression.charAt(i+2)=='s'){
                //System.out.println(expression);
                int nn = nodeNumber(expression,i+4);
                //System.out.println("cos:"+nn);
                int a = ("os;"+nn+";").length();
                Node n1 = nodes.get(nn);
                Node n = new Cosine(n1);
                nodes.add(n);
                String node = ";"+(nodes.size()-1)+";";
                expression = expression.replace("cos;"+nn+";",node);
                i+=node.length()-1;
            }else if(c=='t'&&expression.charAt(i+1)=='a'&&expression.charAt(i+2)=='n'){
                //System.out.println("tan");
                int nn = nodeNumber(expression,i+4);
                int a = ("an;"+nn+";").length();
                Node n1 = nodes.get(nn);
                Node n = new Tangent(n1);
                nodes.add(n);
                String node = ";"+(nodes.size()-1)+";";
                expression = expression.replace("tan;"+nn+";",node);
                i+=node.length()-1;
            }else if(c=='a'&&expression.charAt(i+1)=='b'&&expression.charAt(i+2)=='s'){
                //System.out.println("tan");
                int nn = nodeNumber(expression,i+4);
                int a = ("bs;"+nn+";").length();
                Node n1 = nodes.get(nn);
                Node n = new Absolute(n1);
                nodes.add(n);
                String node = ";"+(nodes.size()-1)+";";
                expression = expression.replace("abs;"+nn+";",node);
                i+=node.length()-1;
            }else if(c=='l'&&expression.charAt(i+1)=='o'&&expression.charAt(i+2)=='g'){
                //System.out.println("tan");
                int nn = nodeNumber(expression,i+4);
                int a = ("og;"+nn+";").length();
                Node n1 = nodes.get(nn);
                Node n = new Log(n1);
                nodes.add(n);
                String node = ";"+(nodes.size()-1)+";";
                expression = expression.replace("log;"+nn+";",node);
                i+=node.length()-1;
            }else if(c=='l'&&expression.charAt(i+1)=='n'){
                //System.out.println("tan");
                int nn = nodeNumber(expression,i+3);
                int a = ("n;"+nn+";").length();
                Node n1 = nodes.get(nn);
                Node n = new NaturalLog(n1);
                nodes.add(n);
                String node = ";"+(nodes.size()-1)+";";
                expression = expression.replace("ln;"+nn+";",node);
                i+=node.length()-1;
            }else if(Character.isLetter(c)){
                if(c=='e'){
                    Node n = new Number(Math.E);
                    nodes.add(n);
                    String node = ";" + (nodes.size() - 1) + ";";
                    expression = expression.replace(Character.toString(c), node);
                    i += node.length() - 1;
                }else {
                    Node n = new Variable(Character.toString(c));
                    nodes.add(n);
                    String node = ";" + (nodes.size() - 1) + ";";
                    expression = expression.replace(Character.toString(c), node);
                    i += node.length() - 1;
                }
            }
            System.out.println(expression);
        }
        System.out.println("2");
        for(int i = 0;i<expression.length();i++){
            char c = expression.charAt(i);
            if(c=='^'){
                System.out.println(expression);
                int n1 = nodeNumberEnd(expression,i-2);
                int n2 = nodeNumber(expression,i+2);
                Node fn = nodes.get(n1);
                Node sn = nodes.get(n2);
                //System.out.println("multiply?"+n1+", "+n2);
                Node n = new Power(fn,sn);
                String sub = ";"+n1+";^;"+n2+";";
                nodes.add(n);
                String node = ";"+(nodes.size()-1)+";";
                expression = expression.replace(sub,node);
                i+=node.length()-5;
            }
            System.out.println(expression);
        }
        System.out.println("3");
        char pc = '1';
        for(int i = 0;i<expression.length();i++){
            char c = expression.charAt(i);
            if(c=='*'){
                int n1 = nodeNumberEnd(expression,i-2);
                int n2 = nodeNumber(expression,i+2);
                Node fn = nodes.get(n1);
                Node sn = nodes.get(n2);
                //System.out.println("multiply?"+n1+", "+n2);
                Node n = new Times(fn,sn);
                String sub = ";"+n1+";*;"+n2+";";
                nodes.add(n);
                String node = ";"+(nodes.size()-1)+";";
                expression = expression.replace(sub,node);
                i+=node.length()-5;
                pc=expression.charAt(i-1);
            }else if(c=='/'){
                int n1 = nodeNumberEnd(expression,i-2);
                int n2 = nodeNumber(expression,i+2);
                //System.out.println(n1+", "+n2);
                Node fn = nodes.get(n1);
                Node sn = nodes.get(n2);
                Node n = new Divide(fn,sn);
                String sub = ";"+n1+";/;"+n2+";";
                nodes.add(n);
                String node = ";"+(nodes.size()-1)+";";
                expression = expression.replace(sub,node);
                i+=node.length()-5;
                pc=expression.charAt(i-1);
            }else if(pc==';'&&c==';'){
                //System.out.println(i);
                //System.out.println(expression);
                int n1 = nodeNumberEnd(expression,i-2);
                int n2 = nodeNumber(expression,i+1);
                Node fn = nodes.get(n1);
                Node sn = nodes.get(n2);
                //System.out.println("multiply?"+n1+", "+n2);
                Node n = new Times(fn,sn);
                String sub = ";"+n1+";;"+n2+";";
                nodes.add(n);
                String node = ";"+(nodes.size()-1)+";";
                expression = expression.replace(sub,node);
                i+=node.length()-4;
                pc=expression.charAt(i-1);
                //System.out.println(expression);
                //System.out.println();
            }
            pc=c;
            System.out.println(expression);
        }
        System.out.println("4");
        for(int i = 0;i<expression.length();i++){
            char c = expression.charAt(i);
            if(c=='+'){
                int n1 = nodeNumberEnd(expression,i-2);
                int n2 = nodeNumber(expression,i+2);
                Node fn = nodes.get(n1);
                Node sn = nodes.get(n2);
                Node n = new Plus(fn,sn);
                String sub = ";"+n1+";+;"+n2+";";
                nodes.add(n);
                String node = ";"+(nodes.size()-1)+";";
                expression = expression.replace(sub,node);
                //System.out.println(nodes.size()-1);
                i+=node.length()-5;
                //System.out.println("char: "+expression.charAt(i)+" expression: "+expression+" number: "+i);
            }else if(c=='-'){
                int n1 = nodeNumberEnd(expression,i-2);
                int n2 = nodeNumber(expression,i+2);
                Node fn = nodes.get(n1);
                Node sn = nodes.get(n2);
                Node n = new Minus(fn,sn);
                String sub = ";"+n1+";-;"+n2+";";
                nodes.add(n);
                String node = ";"+(nodes.size()-1)+";";
                expression = expression.replace(sub,node);
                i+=node.length()-5;
                //System.out.println("char: "+expression.charAt(i)+" expression: "+expression+" number: "+i);
            }
            System.out.println(expression);
        }
        System.out.println("5");
        //System.out.println(expression);
        int n = nodeNumber(expression,1);
        //System.out.println(n);
        System.out.println("-----");
        return nodes.get(n);
    }
    private static int nodeNumber(String expression, int start){
        //System.out.println("nn");
        //System.out.println(expression);
        //System.out.println(expression.charAt(start));
        for(int i = start;i<expression.length();i++){
            if(expression.charAt(i)==';'){
                String sub = expression.substring(start,i);
                return Integer.parseInt(sub);
            }
        }
        return -1;
    }
    private static int nodeNumberEnd(String expression, int end){
        //System.out.println("enn");
        //System.out.println(expression);
        //System.out.println(expression.charAt(end));
        for(int i = end;i>=0;i--){
            //System.out.println(expression.charAt(i));
            if(expression.charAt(i)==';'){
                String sub = expression.substring(i+1,end+1);
                //System.out.println("ennfin");
                return Integer.parseInt(sub);
            }
        }
        return -1;
    }
    public static  String nts(Node n){
        String ret = nodeToExpression(n);
        ret = ret.replace("--", "+");
        ret = ret.replace("++", "+");
        ret = ret.replace("+-", "-");
        ret = ret.replace("-+", "-");
        return ret;
    }
    public static String nodeToExpression(Node n){
        if(n.getClass().equals(Plus.class)){
            Plus a = (Plus)n;
            return "("+nodeToExpression(a.n1)+"+"+nodeToExpression(a.n2)+")";
        }else if(n.getClass().equals(Minus.class)){
            Minus a = (Minus)n;
            return "("+nodeToExpression(a.n1)+"-"+nodeToExpression(a.n2)+")";
        }else if(n.getClass().equals(Times.class)){
            Times a = (Times)n;
            return ""+nodeToExpression(a.n1)+"*"+nodeToExpression(a.n2)+"";
        }else if(n.getClass().equals(Divide.class)){
            Divide a = (Divide)n;
            return ""+nodeToExpression(a.n1)+"/"+nodeToExpression(a.n2)+"";
        }else if(n.getClass().equals(Power.class)){
            Power a = (Power)n;
            return "("+nodeToExpression(a.n1)+")^("+nodeToExpression(a.n2)+")";
        }else  if(n.getClass().equals(Sine.class)){
            Sine a = (Sine)n;
            return "sin("+nodeToExpression(a.n1)+")";
        }else if(n.getClass().equals(Cosine.class)){
            Cosine a = (Cosine)n;
            return "cos("+nodeToExpression(a.n1)+")";
        }else if(n.getClass().equals(Tangent.class)){
            Tangent a = (Tangent)n;
            return "tan("+nodeToExpression(a.n1)+")";
        }else if(n.getClass().equals(Absolute.class)){
            Absolute a = (Absolute)n;
            return "abs("+nodeToExpression(a.n1)+")";
        }else if(n.getClass().equals(Log.class)){
            Log a = (Log)n;
            return "log("+nodeToExpression(a.n1)+")";
        }else if(n.getClass().equals(NaturalLog.class)){
            NaturalLog a = (NaturalLog)n;
            return "ln("+nodeToExpression(a.n1)+")";
        }else if(n.getClass().equals(Number.class)){
            Number a = (Number)n;
            return Double.toString(a.getValue(null));
        }else if(n.getClass().equals(Variable.class)){
            Variable a = (Variable)n;
            return a.letter;
        }else{
            return "";
        }
    }
    public static Node ds(Node n){
        return simplify(simplify(n));
    }
    public static Node simplify(Node n){
        if(n.getClass().equals(Plus.class)){
            Plus a = (Plus)n;
            if(a.n1.getClass().equals(Number.class)){
                Number n1 = (Number)a.n1;
                if(n1.getValue(null)==0) {
                    return simplify(a.n2);
                }
            }else if(a.n2.getClass().equals(Number.class)){
                Number n2 = (Number)a.n2;
                if(n2.getValue(null)==0) {
                    return simplify(a.n1);
                }
            }
            try {
                return new Number(a.getValue(null));
            }catch(Exception e){
                return new Plus(simplify(a.n1),simplify(a.n2));
            }
        }else if(n.getClass().equals(Minus.class)){
            Minus a = (Minus)n;
            if(a.n1.getClass().equals(Number.class)){
                Number n1 = (Number)a.n1;
                if(n1.getValue(null)==0) {
                    return simplify(new Times(new Number(-1),a.n2));
                }
            }else if(a.n2.getClass().equals(Number.class)){
                Number n2 = (Number)a.n2;
                if(n2.getValue(null)==0) {
                    return simplify(a.n1);
                }
            }
            try {
                return new Number(a.getValue(null));
            }catch(Exception e){
                return new Minus(simplify(a.n1),simplify(a.n2));
            }
        }else if(n.getClass().equals(Times.class)){
            Times a = (Times)n;
            if(a.n1.getClass().equals(Number.class)){
                Number n1 = (Number)a.n1;
                if(n1.getValue(null)==0) {
                    return new Number(0);
                }else if(n1.getValue(null)==1){
                    return a.n2;
                }
            }else if(a.n2.getClass().equals(Number.class)){
                Number n2 = (Number)a.n2;
                if(n2.getValue(null)==0) {
                    return new Number(0);
                }else if(n2.getValue(null)==1){
                    return a.n1;
                }
            }
            try {
                return new Number(a.getValue(null));
            }catch(Exception e){
                return new Times(simplify(a.n1),simplify(a.n2));
            }
        }else if(n.getClass().equals(Divide.class)){
            Divide a = (Divide)n;
            if(a.n1.getClass().equals(Number.class)){
                Number n1 = (Number)a.n1;
                if(n1.getValue(null)==0) {
                    return new Number(0);
                }
            }else if(a.n2.getClass().equals(Number.class)){
                Number n2 = (Number)a.n2;
                if(n2.getValue(null)==0) {
                    return null;
                }
            }
            try {
                return new Number(a.getValue(null));
            }catch(Exception e){
                return new Divide(simplify(a.n1),simplify(a.n2));
            }
        }else if(n.getClass().equals(Power.class)){
            Power a = (Power)n;
            if(a.n2.getClass().equals(Number.class)){
                Number n2 = (Number)a.n2;
                if(n2.getValue(null)==0) {
                    return new Number(1);
                }else if(n2.getValue(null)==1){
                    return a.n1;
                }
            }
            try {
                return new Number(a.getValue(null));
            }catch(Exception e){
                return new Power(simplify(a.n1),simplify(a.n2));
            }
        }else  if(n.getClass().equals(Sine.class)){
            Sine a = (Sine)n;
            try {
                return new Number(a.getValue(null));
            }catch(Exception e){
                return new Sine(simplify(a.n1));
            }
        }else if(n.getClass().equals(Cosine.class)){
            Cosine a = (Cosine)n;
            try {
                return new Number(a.getValue(null));
            }catch(Exception e){
                return new Cosine(simplify(a.n1));
            }
        }else if(n.getClass().equals(Tangent.class)){
            Tangent a = (Tangent)n;
            try {
                return new Number(a.getValue(null));
            }catch(Exception e){
                return new Tangent(simplify(a.n1));
            }
        }else  if(n.getClass().equals(Absolute.class)){
            Absolute a = (Absolute)n;
            try {
                return new Number(a.getValue(null));
            }catch(Exception e){
                return new Absolute(simplify(a.n1));
            }
        }else if(n.getClass().equals(Log.class)){
            Log a = (Log)n;
            try {
                return new Number(a.getValue(null));
            }catch(Exception e){
                return new Log(simplify(a.n1));
            }
        }else if(n.getClass().equals(NaturalLog.class)){
            NaturalLog a = (NaturalLog)n;
            try {
                return new Number(a.getValue(null));
            }catch(Exception e){
                return new NaturalLog(simplify(a.n1));
            }
        }else if(n.getClass().equals(Number.class)){
            Number a = (Number)n;
            return new Number(a.getValue(null));
        }else if(n.getClass().equals(Variable.class)){
            return n;
        }else{
            return n;
        }
    }
    /*public static void main(String[] args){
        //Node n = nodeFromString("1+2+4");
        //Node n = nodeFromString("x^2+y^3");
        Node dn = differentiateNode(n);
        //System.out.println(nts(n));
        //System.out.println(nts(dn));
        n = simplify(simplify(n));
        dn = simplify(simplify(dn));
        System.out.println(nts(n));
        System.out.println(nts(dn));
        HashMap<String,Double> vars = new HashMap<String,Double>();
        vars.put("y", new Double(0));
        for(int a = -10;a<=10;a++) {
            vars.put("x",new Double(a));
            try {
                System.out.println(n.getValue(vars) + " : " + dn.getValue(vars));
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        //nodeFromString("tan(1)");
    }*/
}
