package com.example.brandon.graphmaster;

/**
 * Created by Brandon on 9/14/2016.
 */
import android.graphics.Color;
import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

public class Triangle {
    /*private final String vertexShaderCode = "attribute vec4 Pos, Color; // vertex shader inputs\n" +
            "varying vec4 vColor;       // vertex shader output\n" +
            "uniform mat4 MVP;          // model-view-projection matrix\n" +
            "void main() {\n" +
            "    gl_Position = MVP * Pos;\n" +
            "    vColor = Color;\n" +
            "}";
    private final String fragmentShaderCode = "varying lowp vec4 vColor;   // vertex shader output\n" +
            "void main() {\n" +
            "    gl_FragColor = vColor;\n" +
            "}";*/
    private final String vertexShaderCode =
            "attribute vec4 vPosition;" +
                    "void main() {" +
                    "  gl_Position = vPosition;" +
                    "}";

    private final String fragmentShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "void main() {" +
                    "  gl_FragColor = vColor;" +
                    "}";
    private FloatBuffer vertexBuffer;

    // number of coordinates per vertex in this array
    static final int COORDS_PER_VERTEX = 3;
    static float triangleCoords[] = {   // in counterclockwise order:
            0.0f,  0.622008459f, 0.0f, // top
            -0.5f, -0.311004243f, 0.0f, // bottom left
            0.5f, -0.311004243f, 0.0f  // bottom right
    };

    // Set color with red, green, blue and alpha (opacity) values
    float color[] = new float[4];
    float color1[] = new float[4];
    float color2[] = new float[4];
    float color3[] = new float[4];
    private final int mProgram;

    public Triangle(Point3D p1, Point3D p2, Point3D p3) {
        // initialize vertex byte buffer for shape coordinates
        triangleCoords[0]=(float)p1.x;
        triangleCoords[1]=(float)p1.y;
        triangleCoords[3]=(float)p2.x;
        triangleCoords[4]=(float)p2.y;
        triangleCoords[6]=(float)p3.x;
        triangleCoords[7]=(float)p3.y;
        color1[0]= Color.red(p1.c)/255.0f;
        color1[1]= Color.green(p1.c)/255.0f;
        color1[2]= Color.blue(p1.c)/255.0f;
        color2[0]= Color.red(p2.c)/255.0f;
        color2[1]= Color.green(p2.c)/255.0f;
        color2[2]= Color.blue(p2.c)/255.0f;
        color3[0]= Color.red(p3.c)/255.0f;
        color3[1]= Color.green(p3.c)/255.0f;
        color3[2]= Color.blue(p3.c)/255.0f;
        color1[3]=1.0f;
        color2[3]=1.0f;
        color3[3]=1.0f;
        color[0]= Color.red(p1.c)/255.0f;
        color[1]= Color.green(p1.c)/255.0f;
        color[2]= Color.blue(p1.c)/255.0f;
        color[3]= 1.0f;
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // (number of coordinate values * 4 bytes per float)
                triangleCoords.length * 4);
        // use the device hardware's native byte order
        bb.order(ByteOrder.nativeOrder());

        // create a floating point buffer from the ByteBuffer
        vertexBuffer = bb.asFloatBuffer();
        // add the coordinates to the FloatBuffer
        vertexBuffer.put(triangleCoords);
        // set the buffer to read the first coordinate
        vertexBuffer.position(0);

        int vertexShader = GlRender.loadShader(GLES20.GL_VERTEX_SHADER,
                vertexShaderCode);
        int fragmentShader = GlRender.loadShader(GLES20.GL_FRAGMENT_SHADER,
                fragmentShaderCode);

        // create empty OpenGL ES Program
        mProgram = GLES20.glCreateProgram();

        // add the vertex shader to program
        GLES20.glAttachShader(mProgram, vertexShader);

        // add the fragment shader to program
        GLES20.glAttachShader(mProgram, fragmentShader);

        // creates OpenGL ES program executables
        GLES20.glLinkProgram(mProgram);
    }
    private int mPositionHandle;
    private int mColorHandle;

    private final int vertexCount = triangleCoords.length / COORDS_PER_VERTEX;
    private final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex

    public void draw() {
        // Add program to OpenGL ES environment
        GLES20.glUseProgram(mProgram);

        // get handle to vertex shader's vPosition member
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle);

        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);

        // get handle to fragment shader's vColor member
        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");

        // Set color for drawing the triangle
        //GLES20.glUniform4fv(mColorHandle, 1, color, 0);

        // Draw the triangle
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertexCount);

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }
}